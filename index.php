<?php
header('Location: http://rodaverde.com');
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));
defined('APP_PATH') || define('APP_PATH', APPLICATION_PATH);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
defined('APP_ENV') || define('APP_ENV', APPLICATION_ENV);
defined('ENV_DEV') || define('ENV_DEV', APP_ENV=='development');
defined('ENV_PRO') || define('ENV_PRO', APP_ENV=='production');
defined('ENV_TEST')|| define('ENV_TEST',APP_ENV=='testing');
defined('ENV_STAG')|| define('ENV_STAG',APP_ENV=='staging');
defined('DS') || define('DS', DIRECTORY_SEPARATOR);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/models'),
    get_include_path(),
)));

function __autoload($class){
    require_once(implode("/",explode("_",$class)).".php");
}

//Is_Var::dump(get_include_path());exit();

/** ******************************************
 * Libs
 ****************************************** */
Lib::import('*');
Lib::import('helper.*');
/** ******************************************
 * /Libs
 ****************************************** */

/** ******************************************
 * Rotas personalizadas
 ****************************************** */
$routes = array(
    'img' => new Zend_Controller_Router_Route('img/:img/*',array(
        'controller' => 'img',
        'action'     => 'index'
    )),
    'min' => new Zend_Controller_Router_Route('min/:type/:files',array(
        'controller' => 'min',
        'action'     => 'index'
    )),
    'busca' => new Zend_Controller_Router_Route('busca/:busca/',array(
        'controller' => 'busca',
        'action'     => 'index'
    )),
    'carros-leves' => new Zend_Controller_Router_Route('carros-leves/:alias/',array(
        'controller' => 'carros-leves',
        'action'     => 'interna'
    )),
    'carros-leves-filtros' => new Zend_Controller_Router_Route('carros-leves/:filtro/:value/',array(
        'controller' => 'carros-leves',
        'action'     => 'index'
    )),
    'carros-leves-filtros2' => new Zend_Controller_Router_Route('carros-leves/:filtro/:value/*',array(
        'controller' => 'carros-leves',
        'action'     => 'index'
    )),
    'cons' => new Zend_Controller_Router_Route('concessionarias/',array(
        'controller' => 'agendar-test-drive',
        'action'     => 'index'
    )),
    'cons-agendar' => new Zend_Controller_Router_Route('concessionarias/:pid/',array(
        'controller' => 'agendar-test-drive',
        'action'     => 'index'
    )),
    'agendar' => new Zend_Controller_Router_Route('agendar-test-drive/:pid/',array(
        'controller' => 'agendar-test-drive',
        'action'     => 'index'
    )),
    'admin-edit' => new Zend_Controller_Router_Route('admin/:controller/edit/:id/',array(
        'module'     => 'admin',
        'controller' => ':controller',
        'action'     => 'edit'
    )),
    'paginas-edit' => new Zend_Controller_Router_Route('admin/paginas/edit/:alias/',array(
        'module'     => 'admin',
        'controller' => 'paginas',
        'action'     => 'edit'
    )),
    
    // blogs
    'admin-blogs-posts-actions' => new Zend_Controller_Router_Route('admin/blogs-posts/:alias/:action/',array(
        'module'     => 'admin',
        'controller' => 'blogs-posts',
        'action'     => ':action'
    )),
    'admin-blogs-posts' => new Zend_Controller_Router_Route('admin/blogs-posts/:alias/',array(
        'module'     => 'admin',
        'controller' => 'blogs-posts',
        'action'     => 'index'
    )),
    'admin-blogs-posts-edit' => new Zend_Controller_Router_Route('admin/blogs-posts/:alias/edit/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-posts',
        'action'     => 'edit'
    )),
    'admin-blogs-posts-del' => new Zend_Controller_Router_Route('admin/blogs-posts/:alias/del/id/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-posts',
        'action'     => 'del'
    )),
    'admin-blogs-posts-deljson' => new Zend_Controller_Router_Route('admin/blogs-posts/:alias/del.json/id/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-posts',
        'action'     => 'del-json'
    )),
    'admin-blogs-posts-deljson2' => new Zend_Controller_Router_Route('admin/blogs-posts/:alias/del-json/id/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-posts',
        'action'     => 'del-json'
    )),
    'admin-blogs-comments-actions' => new Zend_Controller_Router_Route('admin/blogs-comments/:alias/:action/',array(
        'module'     => 'admin',
        'controller' => 'blogs-comments',
        'action'     => ':action'
    )),
    'admin-blogs-comments' => new Zend_Controller_Router_Route('admin/blogs-comments/:alias/',array(
        'module'     => 'admin',
        'controller' => 'blogs-comments',
        'action'     => 'index'
    )),
    'admin-blogs-comments-edit' => new Zend_Controller_Router_Route('admin/blogs-comments/:alias/edit/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-comments',
        'action'     => 'edit'
    )),
    'admin-blogs-comments-del' => new Zend_Controller_Router_Route('admin/blogs-comments/:alias/del/id/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-comments',
        'action'     => 'del'
    )),
    'admin-blogs-comments-deljson' => new Zend_Controller_Router_Route('admin/blogs-comments/:alias/del-json/id/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-comments',
        'action'     => 'del-json'
    )),
    'admin-blogs-comments-deljson' => new Zend_Controller_Router_Route('admin/blogs-comments/:alias/del.json/id/:id/',array(
        'module'     => 'admin',
        'controller' => 'blogs-comments',
        'action'     => 'del-json'
    )),
    'blog-categoria' => new Zend_Controller_Router_Route('blog/categoria/:categoria/',array(
        'controller' => 'blog',
        'action'     => 'index'
    )),
    'blog1-categoria' => new Zend_Controller_Router_Route('tendencias-mobilidade/categoria/:categoria/',array(
        'controller' => 'tendencias-mobilidade',
        'action'     => 'index'
    )),
    'blog2-categoria' => new Zend_Controller_Router_Route('blog-noticias/categoria/:categoria/',array(
        'controller' => 'blog-noticias',
        'action'     => 'index'
    )),
    'blog-busca' => new Zend_Controller_Router_Route('blog/busca/:busca/',array(
        'controller' => 'blog',
        'action'     => 'index'
    )),
    'blog-arquivo' => new Zend_Controller_Router_Route('blog/arquivo/:arquivo/',array(
        'controller' => 'blog',
        'action'     => 'index'
    )),
    // 'blog-post-id' => new Zend_Controller_Router_Route('blog/:noticia/:id',array(
    //     'controller' => 'blog',
    //     'action'     => 'index'
    // )),
    'blog-post' => new Zend_Controller_Router_Route('blog/:noticia',array(
        'controller' => 'blog',
        'action'     => 'index'
    )),
    'blog1-post' => new Zend_Controller_Router_Route('tendencias-mobilidade/:noticia',array(
        'controller' => 'tendencias-mobilidade',
        'action'     => 'index'
    )),
    'blog2-post' => new Zend_Controller_Router_Route('blog-noticias/:noticia',array(
        'controller' => 'blog-noticias',
        'action'     => 'index'
    )),
    'blog-comment' => new Zend_Controller_Router_Route('blog/comentar',array(
        'controller' => 'blog',
        'action'     => 'comentar'
    )),
    'blog-comment-json' => new Zend_Controller_Router_Route('blog/comentar.json',array(
        'controller' => 'blog',
        'action'     => 'comentar-json'
    )),
    'blog-comment-json2' => new Zend_Controller_Router_Route('blog/comentar-json',array(
        'controller' => 'blog',
        'action'     => 'comentar-json'
    )),
);

// routes admin fornecedores
$routes_forns = array('vp','cr','ps','pt','mt');
foreach ($routes_forns as $rf) {
    $routes['fornecedores-'.$rf] = new Zend_Controller_Router_Route('admin/fornecedores-'.$rf.'/',array(
        'module'     => 'admin',
        'controller' => 'fornecedores',
        'tipo'       => $rf
    ));
    $routes['fornecedores-'.$rf.'-actions'] = new Zend_Controller_Router_Route('admin/fornecedores-'.$rf.'/:action',array(
        'module'     => 'admin',
        'controller' => 'fornecedores',
        'tipo'       => $rf
    ));
    $routes['fornecedores-'.$rf.'-edit'] = new Zend_Controller_Router_Route('admin/fornecedores-'.$rf.'/edit/:id',array(
        'module'     => 'admin',
        'controller' => 'fornecedores',
        'action'     => 'edit',
        'tipo'       => $rf
    ));
    $routes['fornecedores-'.$rf.'-del'] = new Zend_Controller_Router_Route('admin/fornecedores-'.$rf.'/del/id/:id',array(
        'module'     => 'admin',
        'controller' => 'fornecedores',
        'action'     => 'del',
        'tipo'       => $rf
    ));
    $routes['fornecedores-'.$rf.'-del-json'] = new Zend_Controller_Router_Route('admin/fornecedores-'.$rf.'/del.json/id/:id',array(
        'module'     => 'admin',
        'controller' => 'fornecedores',
        'action'     => 'del',
        'tipo'       => $rf
    ));
}

// routes paginas de texto
$routes_pags = array(
    'dirigir-carro-eletrico' => array(
        'beneficios-gerais',
        'transformar-carro-em-eletrico',
    ),
    'carregar' => array(
        'o-basico',
        'padroes',
    ),
    'paineis-solares' => array(
        'o-basico',
        'investimento',
    ),
    'empresa' => array(
        'quem-somos',
        'torne-se-um-parceiro',
        'faq',
        // 'contato',
        'tos',
    ),
);
foreach($routes_pags as $rpK => $rps) {
    $hasRedir1 = !in_array($rpK, array('paineis-solares'));
    if($hasRedir1) $routes['pagina-'.$rpK] = new Zend_Controller_Router_Route($rpK.'/',array(
        'controller' => 'paginas',
        'alias1'     => $rpK,
    ));

    foreach($rps as $rpV) {
        $routes['pagina-'.$rpK.'-'.$rpV] = new Zend_Controller_Router_Route($rpK.'/'.$rpV,array(
            'controller' => 'paginas',
            'alias1'      => $rpK,
            'alias'       => $rpV,
        ));
    }
}
// _d($routes);

$front = Zend_Controller_Front::getInstance();
$router = $front->getRouter();
$router->addRoutes($routes);
/** ******************************************
 * /Rotas personalizadas
 ****************************************** */

$router = new Zend_Controller_Router_Rewrite();
$request = new Zend_Controller_Request_Http();
$router->route($request);

/** ******************************************
 * Defines
 ****************************************** */
defined('SITE_NAME')
    || define('SITE_NAME', "rodaverde");

defined('SITE_TITLE')
    || define('SITE_TITLE', "Roda Verde");

defined('SITE_DESCRIPTION')
    || define('SITE_DESCRIPTION', "");
    
defined('SITE_KEYWORDS')
    || define('SITE_KEYWORDS', "");

// defined('SITE_ANALYTICS')
//     || define('SITE_ANALYTICS', "");

defined('SITE_ADSENSE')
    || define('SITE_ADSENSE', "ca-pub-9488041009256964");

defined('ROOT_PATH')
    || define('ROOT_PATH', $request->getBaseUrl());

defined('IS_DOMAIN')
    || define('IS_DOMAIN', !(bool)strstr(ROOT_PATH, SITE_NAME) && !(bool)strstr(ROOT_PATH, 'previa'));

defined('IS_SSL')
    || define('IS_SSL', (bool)$_SERVER['HTTPS']);

defined('URL')
    || define('URL', (IS_SSL ? 'https' : 'http')."://".$_SERVER['HTTP_HOST'].ROOT_PATH);

defined('FULL_URL')
    || define('FULL_URL', strtolower(URL.str_replace(ROOT_PATH.'','',$_SERVER['REQUEST_URI'])));

defined('SCRIPT_MIN')
    || define('SCRIPT_MIN', 'load'); // 'load' or 'min'

defined('SCRIPT_RETURN_PATH')
    || define('SCRIPT_RETURN_PATH', IS_DOMAIN ? '.' : '..');

defined('JS_PATH')
    || define('JS_PATH', ROOT_PATH.'/public/js');
defined('FULL_JS_PATH')
    || define('FULL_JS_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".JS_PATH);
define('JS_URL',URL.'/public/js');

defined('CSS_PATH')
    || define('CSS_PATH', ROOT_PATH.'/public/css');
defined('FULL_CSS_PATH')
    || define('FULL_CSS_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".CSS_PATH);
define('CSS_URL',URL.'/public/css');
    
defined('IMG_PATH')
    || define('IMG_PATH', ROOT_PATH.'/public/images');
defined('FULL_IMG_PATH')
    || define('FULL_IMG_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH);
define('IMG_URL',URL.'/public/images');
    
defined('FILE_PATH')
    || define('FILE_PATH', ROOT_PATH.'/public/files');
defined('FULL_FILE_PATH')
    || define('FULL_FILE_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH);
define('FILE_URL',URL.'/public/files');

defined('GMAPS_API_KEY')
    || define('GMAPS_API_KEY', 'AIzaSyCGmrF4IJV3y7XTU3EOKe96fvw11vB8T6c');

defined('DEFAULT_LANGUAGE')
    || define('DEFAULT_LANGUAGE', 'pt');
    
/** ******************************************
 * /Defines
 ****************************************** */

/** Zend_Application */
//require_once 'Zend/Application.php';

// load configuration
$config = new Zend_Config_Ini('./application/configs/application.ini',APPLICATION_ENV);

// setup database
$dbAdapter = Zend_Db::factory(
    $config->resources->db->adapter,
    $config->resources->db->params->toArray()
);
Zend_Db_Table::setDefaultAdapter($dbAdapter);

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
            
