<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }
}

class Youtube
{
    public function checkStr($str)
    {
        $regex = "#youtu(be.com|.b)(/v/|/watch\\?v=|e/|/watch(.+)v=)(.{11})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[4][0];
    }

    public function embedUrl($code,$params=null)
    {
        $url = 'http://www.youtube.com/embed/'.$code;
        if($params) $url.= '?'.$params;
        return $url;
    }

    public function embed($code,$width=640,$height=480)
    {
        $url = 'http://www.youtube.com/embed/'.$code.'?autohide=1';
        return '<iframe '.
                'width="'.$width.'" '.
                'height="'.$height.'" '.
                'src="'.$url.'" '.
                'frameborder="0" '.
                'allowfullscreen></iframe>';
    }

    /**
     * @param string $n - 0,1,2,3 | default | mqdefault | maxresdefault
     */
    public function thumbnail($url=null,$n='mqdefault')
    {
        // $queryString = parse_url($url ? $url : self::getUrl(), PHP_URL_QUERY);
        // parse_str($queryString, $params);
        // return "i3.ytimg.com/vi/{$params['v']}/default.jpg";
        return 'http://i3.ytimg.com/vi/'.self::getCode($url).'/'.$n.'.jpg';
    }
    
    public function parseStr($str,$params=array())
    {
        $o = new stdClass();
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        $o->embed = (isset($params['embed_width']) && isset($params['embed_height'])) ? 
                    self::embed($o->code,$params['embed_width'],$params['embed_height']) : 
                    self::embed($o->code);
        $o->embedUrl  = self::embedUrl($o->code);
        $o->thumbnail = self::thumbnail($o->url,(isset($params['thumbnail_youtube']) ? $params['thumbnail_youtube'] : 'mqdefault'));
        return $o;
    }
}

class Vimeo
{
    public function checkStr($str)
    {
        $regex = "#vimeo(.com|.b)(/)(\d{4,})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[3][0];
    }

    public function embed($code,$width=640,$height=480)
    {
        $url = 'http://player.vimeo.com/video/'.$code.'';
        return '<iframe '.
                'width="'.$width.'" '.
                'height="'.$height.'" '.
                'src="'.$url.'" '.
                'frameborder="0" '.
                'allowfullscreen></iframe>';
    }

    public function thumbnail($url=null,$n='thumbnail_medium')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://vimeo.com/api/v2/video/'.self::getCode($url).'.php');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = unserialize(curl_exec($ch));
        $output = $output[0];
        curl_close($ch);
        return $output[$n];
    }
    
    public function parseStr($str)
    {
        $o = new stdClass();
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        $o->embed = (isset($params['embed_width']) && isset($params['embed_height'])) ? 
                    self::embed($o->code,$params['embed_width'],$params['embed_height']) : 
                    self::embed($o->code);
        $o->thumbnail = self::thumbnail($o->url,(isset($params['thumbnail_vimeo']) ? $params['thumbnail_vimeo'] : 'thumbnail_medium'));
        return $o;
    }
}

function _checkVideoStr($str,$params=array()){
    if(strstr($str,'vimeo.com')) return Vimeo::parseStr($str,$params);
    if(strstr($str,'youtube.com')) return Youtube::parseStr($str,$params);
    return false;
}

function _iframeVideo($url,$width=640,$height=480){
    if(strstr($url,'vimeo.com')) return Vimeo::embed(Vimeo::getCode($url),$width,$height);
    if(strstr($url,'youtube.com')) return Youtube::embed(Youtube::getCode($url),$width,$height);
    return false;
}

function _d($var,$exit=1){ return Is_Var::dump($var,$exit); }
function _dr($var,$exit=1){ ob_start(); Is_Var::dump($var,$exit); $ret = ob_get_contents(); ob_end_clean(); echo str_replace("=>\n  ", ' => ', $ret); }
function _e($var,$exit=1){ return Is_Var::export($var,$exit); }

function export_url($section,$ext)
{
    $url = explode('/'.$section,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return implode('/',array($url[0],$section,'export.'.$ext.$url[1]));
}
function export_url_report($section,$action,$ext)
{
    $url = explode('/'.$section.'/'.$action,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    $qs  = (bool)trim($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'].'&' : '?';
    return implode('/',array($url[0],$section,$action)).$qs.'export='.$ext;
}

function _currency($v,$view=null)
{
    return $view ? $view->currency($v) : 'R$ '.number_format($v,2,',','.');
}

function _currencyParcel($v,$parc,$view=null)
{
    return $view ? $view->currency($v/$parc) : 'R$ '.number_format($v/$parc,2,',','.');
}

function _utfRow($row){ return Is_Array::utf8DbRow($row); }

function _utfRows($rows){ return Is_Array::utf8DbResult($rows); }

function appQs($key='',$value='')
{
    $qs = $key.'='.$value;
    $s = strstr(FULL_URL, '?') ? '&' : '?';
    return FULL_URL.$s.$qs;
}

function cleanHtml($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
        }
    }
    
    return $html->html();
    // return $text;
}

function gmapsGeocode($cep)
{
    // $url = 'https://maps.google.com/maps/api/geocode/json?sensor=false'.
    $url = 'https://maps.googleapis.com/maps/api/geocode/json'
        .'?address='.$cep
        .'&sensor=false'
        .'&key='.GMAPS_API_KEY;
    
    $ret = json_decode(file_get_contents($url), true);
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $ret = json_decode(curl_exec($ch), true);
    // _d($ret,0);

    if($ret['status']!='OK' || !(bool)$ret['results']) return null;
    
    return $ret;
}

function gmapsLatLng($cep)
{
    $geo = gmapsGeocode($cep);
    if(!$geo) return null;
    // _d($geo,0);
    return $geo['results'][0]['geometry']['location'];
}

function gmapsDistance($origin,$destination)
{
    $url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
        .'?origins='.$origin
        .'&destinations='.$destination
        // .'&units=imperial'
        .'&key='.GMAPS_API_KEY;
    // _d($url);
    
    $ret = json_decode(file_get_contents($url), true);
    // _d($ret,0);

    if($ret['status']!='OK' || !(bool)$ret['rows']) return null;
    foreach($ret['rows'][0]['elements'] as $elm) if($elm['status']!='OK') return null;
    
    return $ret['rows'][0]['elements'];
}

function cmpDist($a,$b)
{
    return $a['dist'] - $b['dist'];
}
function cmpDistObj($a,$b)
{
    return $a->dist - $b->dist;
}

function _parseEndereco($dados)
{
    $endereco = $dados->endereco;
    $bairro = $dados->bairro;
    $cidade = $dados->cidade;
    $estado = isset($dados->uf) ? $dados->uf : $dados->estado;

    $endereco = reset(explode(' cj.', $endereco));
    $endereco = reset(explode(' cj ', $endereco));
    $endereco = reset(explode(' conj.', $endereco));
    $endereco = reset(explode(' conj ', $endereco));
    $endereco = reset(explode(' Cj.', $endereco));
    $endereco = reset(explode(' Cj ', $endereco));
    $endereco = reset(explode(' Conj.', $endereco));
    $endereco = reset(explode(' Conj ', $endereco));
    $endereco = reset(explode(' - ', $endereco));
    $bairro = reset(explode(' - ', $bairro));
    
    return implode(', ',array($endereco,$bairro,$cidade,$estado));
}

function pedidoDataHora($data)
{
    if(!(bool)trim(@$data)) return array('','','');
    
    $pedido_datahora = $data;
    $p_datahora = explode(' ',$pedido_datahora);
    $p_data = Is_Date::am2br($p_datahora[0]);
    $p_hora = substr($p_datahora[1],0,-3);
    $p_datahora = $p_data.' '.$p_hora;

    return array($p_data,$p_hora,$p_datahora);
}

function pedidoFormaEntrega($taxas)
{
    $forma = '';

    foreach($taxas as $taxa)
        if(strstr($taxa->descricao,'orreio'))
            $forma = trim(end(explode('-',$taxa->descricao)));

    return $forma;
}

function check_url_http($url,$pattern='://')
{
    return strstr($url, $pattern) ? $url : 'http://'.$url;
    // return strstr($url, 'http') ? $url : 'http://'.$url;
}

class Urip
{
    public $uri = FULL_URL;
    public $controller = null;
    public $action = null;

    public function __construct($controller=null,$action=null)
    {
        if($controller) $this->controller = $controller;
        if($action) $this->action = $action;
    }

    public function uri()
    {
        $uri = URL;
        if($this->controller) $uri.= '/'.$this->controller;
        if($this->action) $uri.= '/'.$this->action;
        return $uri;
    }

    public function get()
    {
        $uri1 = $this->uri();
        
        $uri2 = end(explode($uri1, $this->uri));
        $uri2s = explode('/', $uri2);
        $uri2s = array_filter($uri2s);
        // _d($uri2s);
        
        $fparams = array(); for($i=0;$i<sizeof($uri2s);$i++) {
            if(@$uri2s[$i]=='') continue;
            $fparams[$uri2s[$i]] = @$uri2s[$i+1];
            $i++;
        }
        // _d(array($uri,$uri1,$uri2,$uri2s,$fparams));
        
        return $fparams;
    }

    public function set($fparams)
    {
        $uri = $this->uri();
        $fp = array();
        // if(array_key_exists('fcharge', $fparams)) if($fparams['fcharge']!='1') _d($fparams);
        
        foreach($fparams as $fpK => $fpV) $fp[] = $fpK.'/'.$fpV;
        if(count($fp)) $uri = $uri.'/'.implode('/', $fp);
        // _d(array($uri,$fparams));
        
        return $uri;
    }

    public function replace($fparams,$append=0,$removeIfExists=0)
    {
        $uri = $this->uri();
        $fp = $this->get();
        
        foreach($fparams as $fpK => $fpV) {
            $v = $fpV;
            
            if($append) if(array_key_exists($fpK, $fp)) 
                // $v = $fp[$fpK]==$v ? $v : $fp[$fpK].'+'.$v;
                $v = strstr($fp[$fpK],$v) ? $fp[$fpK] : $fp[$fpK].'+'.$v;

            if($removeIfExists) {
                $vs_new = array();
                $vs = explode('+',$v);
                foreach($vs as $vsV) if($vsV!=$fpV) $vs_new[] = $vsV;
                $v = implode('+',$vs_new);
            }
            
            $fp[$fpK] = $v;
        }

        // filtra campos vazios
        $fpChargeInUrl = strstr(FULL_URL, '/fcharge');
        $fpChargeEmpty = @$fp['fcharge']=='';
        $fpChargeNull  = @$fp['fcharge']===null;
        $fpCharge0  = @$fp['fcharge']=='0';
        $fpCharge1  = @$fp['fcharge']=='1';
        $fpCharge01 = @$fp['fcharge']=='1+0' || @$fp['fcharge']=='0+1';

        if(array_key_exists('fcharge', $fp) && !$fpChargeEmpty && !$fpChargeNull){
            // _dr(array('fpChargeInUrl'=>$fpChargeInUrl,'fpChargeEmpty'=>$fpChargeEmpty,'fpChargeNull'=>$fpChargeNull,'fpCharge0'=>$fpCharge0,'fpCharge1'=>$fpCharge1,'fpCharge01'=>$fpCharge01),0);
            
            $fp1 = $this->get();
            $fp1ChargeEmpty = @$fp1['fcharge']=='';
            $fp1ChargeNull  = @$fp1['fcharge']===null;
            $fp1Charge0  = @$fp1['fcharge']=='0';
            $fp1Charge1  = @$fp1['fcharge']=='1';
            $fp1Charge01 = @$fp1['fcharge']=='1+0' || @$fp1['fcharge']=='0+1';
            // _dr(array('fpChargeInUrl'=>$fpChargeInUrl,'fp1ChargeEmpty'=>$fp1ChargeEmpty,'fp1ChargeNull'=>$fp1ChargeNull,'fp1Charge0'=>$fp1Charge0,'fp1Charge1'=>$fp1Charge1,'fp1Charge01'=>$fp1Charge01),0);

            $fparamsChargeEmpty = @$fparams['fcharge']=='';
            $fparamsChargeNull  = @$fparams['fcharge']===null;
            $fparamsCharge0  = @$fparams['fcharge']=='0';
            $fparamsCharge1  = @$fparams['fcharge']=='1';
            $fparamsCharge01 = @$fparams['fcharge']=='1+0' || @$fparams['fcharge']=='0+1';
            // _dr(array('fpChargeInUrl'=>$fpChargeInUrl,'fparamsChargeEmpty'=>$fparamsChargeEmpty,'fparamsChargeNull'=>$fparamsChargeNull,'fparamsCharge0'=>$fparamsCharge0,'fparamsCharge1'=>$fparamsCharge1,'fparamsCharge01'=>$fparamsCharge01),0);

            // if($fp['fcharge']=='') _d($fp,0);
            // if($fp['fcharge']=='') unset($fp['fcharge']);
            if($fp['fcharge']=='') $fp['fcharge'] = '0';
            // _d(@$fp['fcharge']==='',0); _d(@$fparams['fcharge']==='0',0);
            // _d(@$fp1['fcharge'],0); _d(@$fp['fcharge'],0); _d(@$fparams['fcharge'],0);

            switch(true){
                case $fpChargeEmpty && $fp1Charge1 && $fparamsCharge1:
                case $fpChargeEmpty && $fp1ChargeEmpty && $fparamsCharge0:
                    unset($fp['fcharge']); break;
                case $fpCharge1 && $fp1ChargeEmpty && $fparamsCharge1 && $fpChargeInUrl:
                    $fp['fcharge'] = '0+1'; break;
            }
            
            /*
            // if($fp['fcharge']==='0' && $fparams['fcharge']==='0') _d('oi');
            if(@$fp1['fcharge']===null && @$fp['fcharge']=='0' && @$fparams['fcharge']=='0') $fp['fcharge'] = '0';
            else if(@$fp['fcharge']=='0' && @$fparams['fcharge']=='0') unset($fp['fcharge']);
            
            if((@$fp1['fcharge']=='1+0' || @$fp1['fcharge']=='0+1') && @$fparams['fcharge']=='1') $fp['fcharge'] = '0';
            else if(@$fp['fcharge']=='0' && @$fparams['fcharge']=='1') unset($fp['fcharge']);
            */
        } else {
            $fp = array_filter($fp);
        }
        // $fp = array_filter($fp);
        // _d($fp);
        
        return $this->set($fp);
    }
}

/**
 * Retorna URL da página para ser usado em templates, email, etc.
 * (não é usada a define URL para ser a mesma em ambos os ambientes)
 */
function site_url($withHttp=true)
{
    return ($withHttp ? (IS_SSL ? 'https://' : 'http://') : '').'www.'.SITE_NAME.'.com';
}

function site_link($anchor=null, $attrs='')
{
    return '<a href="'.site_url().'" '.$attrs.'>'.($anchor ? $anchor : site_url(false)).'</a>';
}

/**
 * Auxliares para emails
 */
function mail_rodape()
{
    return '<br>'.
           // '<p>Em caso de dúvidas, por favor entre em contato através de nosso email <a href="mailto:contato@'.SITE_NAME.'.com.br">contato@'.SITE_NAME.'.com.br</a>.<br><br>'.
           // 'Horário de atendimento, de segunda à sexta das 10:00 as 16:00.</p><br>'.
           '<p>Atenciosamente,<br>'.SITE_TITLE.'<br>'.site_link().'</p>';
}


function db_table($name='')
{
    if(Zend_Registry::isRegistered('db_'.$name))
        return Zend_Registry::get('db_'.$name);

    $nameCased = explode('_', $name);
    $nameCased = array_map('ucfirst', $nameCased);
    $tableClass = 'Application_Model_Db_'.implode('',$nameCased);
    $table = new $tableClass();
    Zend_Registry::set('db_'.$name, $table);
    return $table;
}
function dbTable($name=''){ return db_table($name); }
// function dbtable($name=''){ return db_table($name); }



