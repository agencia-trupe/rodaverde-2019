// console.log('forns-forms');

$('.forn-form .submit').click(function(e){
	var $form = $(this).parent();
	$form.submit();
});

$('.forn-form form').submit(function(e){
	e.preventDefault();

	var $this = $(this),
		$status = $('.status',$this),
		url = this.action+'.json',
		data = {
			'tema_id' : $('#tema_id',$this).val(),
			'tema'    : $('#tema',$this).val(),
			'nome'    : $('#nome',$this).val(),
			'email'   : $('#email',$this).val(),
			'telefone': $('#telefone',$this).val(),
			'mensagem': $('#mensagem',$this).val()
		},
		err = 0, k, v;

	$status.removeClass('error');
	$status.text('Aguarde...');

	for(k in data) {
		v = data[k];
		if(v=='') err = 1;
	};
	if(err) {
		$status.addClass('error').text('* Preencha todos os campos corretamente');
		return false;
	}

	// console.log([url,data]); return;
	// _sto(function() { $status.text('Formulário enviado!'); },1);
	$.post(url,data,function(json){
		if(json.error){
			$status.addClass('error').text(json.error);
			return;
		}
		$status.text(json.msg);
	},'json');

	return false;
});


function _test() {
	var $row = $('.forn-form').get(0),
		$form = $('form',$row),
		list_alias = CONTROLLER;
	
	if(CONTROLLER=='veiculos-pesados') list_alias = 'fornecedores';
	if(CONTROLLER=='paineis-solares') list_alias = 'paineis';
	
	$('#'+list_alias+'-list li:last-child .agendar-visita').trigger('click');
	$('#nome',$form).val('teste '+CONTROLLER);
	$('#email',$form).val('testeforns@mailinator.com');
	$('#telefone',$form).val('(11) 99999-9999');
	$('#mensagem',$form).val('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro saepe ipsum dolor recusandae sapiente consequatur earum ratione, deserunt iure. Reiciendis dolorem molestias, veniam nobis necessitatibus temporibus quis delectus. Ratione, assumenda?');
}
// if(ENV_DEV) _sto(_test,.5);
