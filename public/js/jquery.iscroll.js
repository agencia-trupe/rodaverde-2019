$.fn.extend({
    iscroll : function(opt){
        var options = {
            controller:null
        }
        
        var conf = opt ? $.extend(options,opt) : options;
        var cursor;
        if($.browser.mozilla){
            cursor = '-moz-grab';
        } else if($.browser.webkit){
            cursor = '-webkit-grab';
        } else if($.browser.opera){
            cursor = '-o-grab';
        } else if($.browser.msie){
            cursor = 'grab';
        } else {
            cursor = 'pointer';
        }
        
        $(this).mousedown(function (event) {
            $(this)
            .data('down', true) // a flag indicating the mouse is down
            .data('x', event.clientX) // the current mouse down X coord
            .data('scrollLeft', this.scrollLeft); // the current scroll position
            
            return false;
        }).mouseup(function (event) {
            $(this).data('down', false);
        }).mousemove(function (event) {
            if ($(this).data('down') == true) {
                this.scrollLeft = $(this).data('scrollLeft') + $(this).data('x') - event.clientX;
            }
        }).css({
            'overflow' : 'hidden', // change to hidden for JS users
            'cursor' : cursor // add the grab cursor
        });
        
        if($(this).mousewheel){
            $(this).mousewheel(function (event, delta) {
                event.preventDefault();
                this.scrollLeft -= (delta * 30);
            })
        }
        
        if(conf.controller){
            var int_tl,
                control = conf.controller,
                $father = $(this);
            
            //$(".presenca-arrows").append("<span id='arrstatus'></status>");
            //$status = $("#arrstatus");
            
            var iscroll_check_scroll_left = function(){
                if($father.scrollLeft() == 0){
                    $(control).find(".scroll-left").addClass("disabled");
                } else {
                    $(control).find(".scroll-left").removeClass("disabled");
                }
                //$status.html($father.scrollLeft());
            }
            
            var iscroll_check_scroll_right = function(){
                //var maxwidth = $father.width() - 30;
                var maxwidth = document.getElementById($father.attr('id')) ?
                                document.getElementById($father.attr('id')).scrollWidth - $father.width() :
                                0;
                if($father.scrollLeft() >= maxwidth){
                    $(control).find(".scroll-right").addClass("disabled");
                } else {
                    $(control).find(".scroll-right").removeClass("disabled");
                }
                
                if($(this).children(">:first-child").width() < $(this).width()){
                    //$(control).find(".scroll-right").addClass("disabled");
                }
                //$status.html($father.scrollLeft()+","+maxwidth);
            }
            
            $(control).find(".scroll-left").mousedown(function(){
                int_tl = setInterval('document.getElementById("'+$father.attr("id")+'").scrollLeft-=30;',100);
            }).mouseup(function(){
                clearInterval(int_tl);
            });
            $(control).find(".scroll-right").mousedown(function(){
                int_tl = setInterval('document.getElementById("'+$father.attr("id")+'").scrollLeft+=30;',100);
            }).mouseup(function(){
                clearInterval(int_tl);
            });
            
            $(this).bind('scroll',function(){
                iscroll_check_scroll_left();
                iscroll_check_scroll_right();
            });
            
            iscroll_check_scroll_left();
            iscroll_check_scroll_right();
        }
    }
});