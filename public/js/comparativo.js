var swiper, 
	slideCount = $('.swiper-slide').size(),
	swiperSpaceBetween = 10,
	swiperSlidesPerView = 3;
if(isMobile) {
	swiperSlidesPerView = isTablet ? 2 : 1;
	swiperSpaceBetween = isTablet ? 10 : 10;
}

if(slideCount>swiperSlidesPerView) {
	$('.swiper-button-next, .swiper-button-prev').show();

	swiper = new Swiper('.swiper-container', {
	    preloadImages: false,
	    grabCursor: true,
	    autoplay: 0,
	    // pagination: '.swiper-pagination',
	    slidesPerView: swiperSlidesPerView,
	    // paginationClickable: true,
	    spaceBetween: swiperSpaceBetween,
	    keyboardControl: false,
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    onSlideChangeEnd: function () {
	        
	    }
	});
}

// remove comparativos
$('.remove-comp').click(function(e){
	if(!confirm('Remover carro do comparativo?')) return false;
	var $this = $(this), $row = $this.parent();
	Comparativo.del($this.data('id'));
	$row.fadeOut(function(){
		$row.remove();
	});
});


var Comparativo = function(){ }

Comparativo.del = function(id){
	var url = URL+'/comparativo/del.json',
		data= {'carro':id};
	$.post(url,data,function(json){
		console.log(json);
	},'json');
}

// mostra detalhes
// var $maisDet = $('#carros-leves .mais-det');
var $maisDet = $('.mais-det');
$maisDet.click(function(e){
	var $this = $(this);

	if(!$this.hasClass('close')){
		// console.log('open');
		$('.carro-detalhes').show();
		// $this.addClass('close');
		$maisDet.addClass('close');
	} else {
		// console.log('close');
		$('.carro-detalhes').hide();
		// $this.removeClass('close');
		$maisDet.removeClass('close');
	}
});