console.log(CONTROLLER);

$('.forms form .submit').click(function(e){
	var $form = $(this).parent();
	$form.submit();
});

$('.forms form').submit(function(e){
	e.preventDefault();

	var $this = $(this),
		$status = $('.status',$this),
		url = this.action+'.json',
		data = $this.serialize(),
		err = 0, k, v;

	$status.removeClass('error');
	$status.text('Aguarde...');

	$('.txt',$this).each(function(i,elm){
		v = elm.value;
		if(v=='') err = 1;
	});
	if(err) {
		$status.addClass('error').text('* Preencha todos os campos corretamente');
		return false;
	}

	console.log([url,data]);
	_sto(function() {
		$status.text('Formulário enviado!');
	},1);

	return false;
});