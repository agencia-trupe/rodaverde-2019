Validator = {
    empty    : function(str){ return $.trim(str).length > 0; },
    number   : function(str){ return (isNaN(str)) ? false : true; },    
    len      : function(str,len){ return str.length == len; },    
    lenMin   : function(str,len){ return str.length >= len; },
    lenMax   : function(str,len){ return str.length <= len; },        
    equal    : function(str1,str2){ return str1 == str2; },
	notEqual : function(str1,str2){ return str1 != str2; },
	mail     : function(str){
        var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?$/);
		return er.test(str);
    }
}
