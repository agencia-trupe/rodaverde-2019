console.log('clicks');

var Rel = {
	
	addClick: function(elm) {
		var $elm = $(elm),
			url  = URL+'/report/click.json'
			data = {
				area_id : $elm.data('area'),
				tema_id : $elm.data('temaid'),
				tema : $elm.data('tema')
			};

		// console.log(url); console.log(data); return;
		$.post(url,data,function(json){
			//
		},'json');

		_sto(function(){
            $elm.removeClass('rel-click');
        },.5);
	},
	
	addAd: function(elm) {
		var $elm = $(elm),
			url  = URL+'/report/ads.json'
			data = {
				id : $elm.data('id')
			};

		// console.log(url); console.log(data); return;
		$.post(url,data,function(json){
			//
		},'json');

		_sto(function(){
            $elm.removeClass('ads-click');
        },.5);
	}

}
