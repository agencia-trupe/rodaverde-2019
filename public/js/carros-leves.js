// console.log(CONTROLLER);

var $comps = $('.car-links .ac'), URI_loaded = 0;
$comps.click(function(e){
	var $this = $(this),
		$btComp = $('.bt-compare-top'),
		hasComps = 0;

	if($this.hasClass('active')){
		Comparativo.del($this.data('id'),function(j){
			// alert(j.msg);
			$this.text('ADICIONAR À COMPARAÇÃO');
			$this.removeClass('active');
			hasComps = $comps.filter('.active').size();
			// if(!hasComps) if(!$btComp.is(':hidden')) $btComp.fadeOut();
			// if(!hasComps) if(!$btComp.is(':hidden')) $btComp.removeClass('active');
			if(!hasComps) $btComp.removeClass('active');
		});
	} else {
		Comparativo.add($this.data('id'),function(j){
			// alert(j.msg);
			$this.text('ADICIONADO À COMPARAÇÃO');
			$this.addClass('active');
			hasComps = $comps.filter('.active').size();
			// if($btComp.is(':hidden')) $btComp.fadeIn();
			// if($btComp.is(':hidden')) $btComp.addClass('active');
			$btComp.addClass('active');
		});
	}
	showBtCompare();
	// console.log(hasComps);
});


if(ACTION=='index'){ // -------------------------------- ACTION index

filtroMenus();
head.js(JS_PATH+'/URI.js',function(){ URI_loaded = 1; });
if(isDesktop) head.js(JS_PATH+'/jquery.ui.min.js',sliders);
if(isMobile)  head.js(JS_PATH+'/jquery.ui.min.js',JS_PATH+'/jquery.ui.touch-punch.min.js',sliders);
window.setTimeout(showBtCompare,500);
$(window).scroll(function(e){
	window.setTimeout(showBtCompare,500);
});

} // --------------------------------------------------- /ACTION index


var Comparativo = function(){ }

Comparativo.add = function(id,fn){
	var url = URL+'/comparativo/add.json',
		data= {'carro':id};
	$.post(url,data,function(json){
		// console.log(json);
		if(typeof(fn)=='function')(fn)(json);
	},'json');
}

Comparativo.del = function(id,fn){
	var url = URL+'/comparativo/del.json',
		data= {'carro':id};
	$.post(url,data,function(json){
		// console.log(json);
		if(typeof(fn)=='function')(fn)(json);
	},'json');
}


function filtroMenus(){
	var $anchors = $('#filtros-list > li > a'),
		$li, $menu, $a;

	$anchors.click(function(e){
		$a = $(this);
		$li = $a.parent();
		$menu = $('ul',$li);

		$a.toggleClass('open');
		if($menu.is(':hidden')) $menu.slideDown();
		else $menu.slideUp();
	});
}

function sliders(){
	if(isMobile) $('.ui-slider-handle').draggable();
	var $slider1 = $("#slider-range"),
		slider1min = Number($slider1.data('min')),
		slider1max = Number($slider1.data('max')),
		$slider2 = $("#slider-range-preco"),
		slider2min = Number($slider2.data('min')),
		slider2max = Number($slider2.data('max'));

	$slider1.slider({
		range: true,
		min: 0, //75,
		max: 650,
		step: 5,
		// values: [ 75, 650 ],
		values: [
			slider1min ? slider1min : 0, //75, 
			slider1max ? slider1max : 650
		],
		slide: function( event, ui ) {
			updRangeRaio(ui.values[0],ui.values[1]);
		},
		stop: function( event, ui ) {
			// console.log(ui.values);
			var uri = URI_loaded ?
				URI().removeSearch('raio')
					 .addSearch('raio',ui.values[0]+'_'+ui.values[1])
					 .toString() :
				appQs('raio',ui.values[0]+'_'+ui.values[1]);
			// console.log(uri);
			window.location.href = uri;
		}
    });
	updRangeRaio(
		$slider1.slider("values",0),
		$slider1.slider("values",1)
	);
	
	$slider2.slider({
		range: true,
		min: 0, //50000,
		max: 1000000, //500000,
		step: 500,
		// values: [ 50000, 500000 ],
		values: [
			slider2min ? slider2min : 0, //50000, 
			slider2max ? slider2max : 1000000 //500000
		],
		slide: function( event, ui ) {
			updRangePreco(ui.values[0],ui.values[1]);
		},
		stop: function( event, ui ) {
			// console.log(ui.values);
			var uri = URI_loaded ?
				URI().removeSearch('preco')
					 .addSearch('preco',ui.values[0]+'_'+ui.values[1])
					 .toString() :
				appQs('preco',ui.values[0]+'_'+ui.values[1]);
			// console.log(uri);
			window.location.href = uri;
		}
    });
	updRangePreco(
		$slider2.slider("values",0),
		$slider2.slider("values",1)
	);
}


function updRangeRaio(v1,v2) {
	$('#filtro-raio-min').text(v1);
	$('#filtro-raio-max').text(v2);
}
function updRangePreco(v1,v2) {
	$('#filtro-preco-min').text(formatMoney(v1));
	$('#filtro-preco-max').text(formatMoney(v2));
}

function formatMoney(v) {
	return v
		.toFixed(2)
		.replace(/\d(?=(\d{3})+\.)/g, '$&,')
		.replace('.',':')
		.replace(/,/g,'.')
		.replace(':',',');
}


function showBtCompare() {
	if($('#top-container').isInViewport()){
		$('.bt-compare-top.active.interna').show();
		$('.bt-compare-top.active.float').hide();
	} else {
		$('.bt-compare-top.active.interna').hide();
		$('.bt-compare-top.active.float').show();
	}
}


