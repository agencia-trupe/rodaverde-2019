// console.log('agendar-test-drive');
// console.log(CONTROLLER);

$('#cons-list .mais-det').click(function(e){
	var $this = $(this),
		$row = $this.parent(),
		$menosdet = $this.next(),
		$open = $('.open',$row),
		$closed = $('.closed',$row);

	$this.fadeOut();
	$menosdet.fadeIn();
	$closed.hide();
	$open.slideDown();
});

$('#cons-list .menos-det').click(function(e){
	var $this = $(this),
		$row = $this.parent(),
		$maisdet = $this.prev(),
		$open = $('.open',$row),
		$closed = $('.closed',$row);

	$this.fadeOut();
	$maisdet.fadeIn();
	$open.slideUp();
	$closed.show();
});


$('.chk-model').click(function(e){
	var $ul = $(this).parent().parent(),
		$pids = $ul.next();
		chks = [];
	$('.chk-model',$ul).each(function(i,elm) {
		if(elm.checked) chks.push(elm.value);
	});
	$pids.val(','+chks.join(',')+',');
});

$('.frm-busca-cons .submit').click(function(e){
	var $form = $(this).parent();
	$form.submit();
});
$('.frm-cons-contato .submit').click(function(e){
	var $form = $(this).parent();
	$form.submit();
});

$('.frm-cons-contato').submit(function(e){
	e.preventDefault();

	var $this = $(this);
	if($this.hasClass('enviando')) return false;
	
	var $status = $('.status',$this),
		url = this.action+'.json',
		data = {
			'tema_id' : $('#cons_id',$this).val(),
			'tema'    : $('#cons',$this).val(),
			'nome'    : $('#nome',$this).val(),
			'email'   : $('#email',$this).val(),
			'telefone': $('#telefone',$this).val(),
			'data'    : $('#data_agendamento',$this).val(),
			'pids'    : $('#prod_ids',$this).val()
		},
		$modList = $('.modelos-list li',$this),
		err = 0, k, v;
	var disable = function(){
			$this.addClass('enviando');
			$('.txt, .submit',$this).addClass('disabled').attr('disabled',true);
			$status.removeClass('error');
			$status.text('Aguarde...');
		},
		enable = function(){
			$this.removeClass('enviando');
			$('.txt, .submit',$this).removeClass('disabled').attr('disabled',false);
		};

	

	disable();

	for(k in data) {
		if(!$modList.size() && k=='pids') continue;
		v = data[k];
		if(v=='') err = 1;
	};
	if(err) {
		enable();
		$status.addClass('error').text('* Preencha todos os campos corretamente');
		return false;
	}

	// console.log(url); console.log(data); return;
	// _sto(function() { $status.text('Formulário enviado!'); },1);
	$.post(url,data,function(json){
		enable();

		if(json.error){
			$status.addClass('error').text(json.error);
			return;
		}
		$status.text(json.msg);
	},'json');

	return false;
});


function _test() {
	var $row = $('#cons-list li:first-child'),
		$form = $('.frm-cons-contato',$row);
	
	$('.mais-det',$row).trigger('click');
	$('#nome',$form).val('teste concessionária');
	$('#email',$form).val('testecons@mailinator.com');
	$('#telefone',$form).val('(11) 99999-9999');
	$('#data_agendamento',$form).val('01/01/2020');
	$('.modelos-list li:first-child .chk-model',$form)
		.attr('checked',true)
		.trigger('click')
		.trigger('change');
}
// if(ENV_DEV) _sto(_test,.5);


