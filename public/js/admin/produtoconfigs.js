var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos=false, active_upload = undefined;

var MsgRow = Messenger;
//MsgRow.container = $(".row-tools");
MsgRow.elm       = ".row-tools .row-status";

$(document).ready(function(){
    ajust_order2();
    
    $('#produtoconfigs .row-list .row-order').live('click',function(){
        if(!$(this).hasClass('disabled')){
            var row = $(this).parent();
            if($(this).hasClass('order-down')){
                row.insertAfter(row.next());
            }else{
                if(row.prev().attr("class") != "title"){
                    row.insertBefore(row.prev());
                }
            }
            ajust_order2();
        }
    });
    
    $('#produtoconfigs .row-list').sortable({
    	items: "li:not(.title)",
        revert:true,
        start:function(e,ui){
            $('#produtoconfigs .row-list li').addClass('disabled');
            ui.item.removeClass('disabled').addClass('grabber');
        },
        stop:function(e,ui){
            $('#produtoconfigs .row-list li').removeClass('disabled').removeClass('grabber');
            ajust_order2();
        }
    });
    // $('#produtoconfigs .row-list li.title').draggable({revert:'invalid'});
    $('#produtoconfigs .row-list li.title').disableSelection();
    
    $('.row-status_id').live('click',function(){
        var row = $(this).parent();
        row.find('input[name="status_id[]"]').val(($(this).is(':checked')?"1":"0"));
    });
	
	$(".row-add").click(function(){
		Rows.add();
	});
	$(".row-action-del").live('click',function(){
		Rows.del($(this).parent());
	});
    $(".frm-produtoconfigs .bt").click(function(){
        Rows.save();
    });
	
	// photo upload
	// $('.row-foto-edit,.row-foto-new').fancybox();
	// $('.row-foto-edit,.row-foto-new').live('click',function(){
	// 	var $this = $(this);
	// 	active_upload = $this.parents('.foto-container');
	// 	$('#file_upload_list .error').remove();
	// 	$('#file_upload').attr('action',[URL,MODULE,DIR,'upload.json'+($this.data('id')?'?id='+$this.data('id'):'')].join('/'));
	// 	//console.log(active_upload);
	// });
	
	Foto.hideNewLinks($('ul.row-list'));
	$('.foto-container').each(function(){
		var $this = $(this),
			$img  = $this.find('img');
		
		if($img.attr('src').indexOf('not-found.jpg') != -1){
			Foto.showNewLinks($this);
		}
	});
	
	// $('.file_upload').fileUploadUI({
 //        uploadTable: $('#file_upload_list'),
 //        downloadTable: $('#file_upload_list'),
 //        buildUploadRow: function (files, index) {
 //            return $('<tr>'+
	// 				'<td class="file_upload_progress"><div></div></td>' +
	// 				'<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
 //                    '<td class="file_upload_cancel">' +
 //                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
 //                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
 //                    '<\/button>' +
	// 				'<\/td>' +
	// 				'<\/tr>');
 //        },
 //        buildDownloadRow: function (file) {
 //            if(file.error) return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			
	// 		active_upload.find('.foto_id').val(file.id);
	// 		active_upload.find('.row-foto').attr('href',URL+'/img/'+DIR+':'+file.name);
	// 		active_upload.find('img').attr('src',URL+'/img/'+DIR+':'+file.name+'?w=20&h=20');
	// 		Foto.hideNewLinks(active_upload);
	// 		$.fancybox.close();
	// 		return '';
 //        },
	// 	beforeSend: function(event, files, index, xhr, handler, callBack){
	// 		var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
	// 			file_name = files[index].name,
	// 			max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
	// 			max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
	// 			file_count= $(".list_fotos li").size(),
	// 			max_count = (max_files>0) ? file_count < max_files : true,
	// 			msg       = "Erro ao enviar imagem";
			
	// 		if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
	// 			callBack();
	// 			return true;
	// 		}
			
	// 		msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
	// 		msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
	// 		msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
			
	// 		handler.cancelUpload(event, files, index, xhr, handler);
	// 		handler.addNode($('#file_upload_list'),
	// 			$('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
	// 		);
	// 	},
	// 	onComplete: function (event, files, index, xhr, handler) {
	// 		$('.fbimg').fancybox();
	// 	}
 //    });
	
	$(".row-foto-del").live('click',function(){
        var row = $(this).parents('.foto-container');
		Files.del(row);
    });
	
	$('.fbimg').fancybox();
	
	_onlyNumbers('.row-desconto');
	
	$('#produtoconfigs .row-list select').each(function(i,v){
		var $this = $(this), old_name = $this.attr('name'), old_id = $this.attr('id');
		$this.attr('name',old_name+'[]');
		$this.attr('id',old_id+'_'+i);
	});
});

// ajusta ordem da lista e habilita/desabilita botões de ordenação
function ajust_order2(){
    $('#produtoconfigs .row-list .row-ordem').each(function(i,v){
        $(this).val(i+1);
    });
    
    $('#produtoconfigs .row-list .row-order').removeClass('disabled');
    $('#produtoconfigs .row-list .order-up:first,#produtoconfigs .row-list .order-down:last').addClass('disabled');
}

Foto = {
	showNewLinks : function(o){
		o.find('.row-foto-edit,.row-foto-del').hide();
		o.find('.row-foto-new').show();
		//o.find('.row-foto').removeClass('fbimg');
	},
	hideNewLinks : function(o){
		o.find('.row-foto-new').hide();
		o.find('.row-foto-edit,.row-foto-del').show();
		o.find('.row-foto').addClass('fbimg');
		$('.fbimg').fancybox();
	}
}

Rows = {
	add: function(){
		var newRow = $("#tmplRow").tmpl().appendTo(".row-list");
		//newRow.find('input').val("");
		newRow.find('input.row-descricao').focus();
        ajust_order2();
		
		Foto.showNewLinks(newRow);
		$('.row-foto-edit,.row-foto-new').fancybox();
	},
	
	del: function(row){
		if(confirm("Deseja remover o registro?")){
			var id        = row.find(".row-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/"+DIR+"/del.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Rows.remove(row);
					}
				});
			} else {
				Rows.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgRow.add("Registro removido.").show(null,3000);
        ajust_order2();
	},
	
	save: function(){
		$('.frm-produtoconfigs').submit();
	}
}

Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        
        if(confirm("Deseja deletar a foto selecionada?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.find('.foto_id').val('0');
					row.find('.row-foto').attr('href',URL+'/img/not-found.jpg');
					row.find('img').attr('src',URL+'/img/not-found.jpg?w=20&h=20');
					Foto.showNewLinks(row);
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}