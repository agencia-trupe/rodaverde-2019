$(document).ready(function(){
    var autosort = ($('.tbl').hasClass('mailling') ? 0:
                    ($('table thead td').length) - 1),
        autosort_order = ($('.tbl').hasClass('mailling') ? 'asc': 'desc');

    head.js(JS_PATH+'/jquery.dataTables.min.js',function(){
        $('.tbl').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            //"bSort": false,
            "bInfo": false,
            "bAutoWidth": false,
            "aaSorting": [[ autosort, autosort_order ]]
        });
    });

    $('.export .print').live('click',function(){ window.print(); });
});