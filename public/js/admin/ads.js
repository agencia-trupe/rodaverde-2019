var MAX_FOTOS = 0, MAX_SIZE = 5242880, allow_photos;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-"+DIR).submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    $("#search-by").change(function(){
        if($(this).val() == "tipo"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(categorias){
                for(i in categorias){
                    $combo.append('<option value="'+i+'">'+categorias[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
});