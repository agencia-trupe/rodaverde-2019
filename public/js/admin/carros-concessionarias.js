console.log(CONTROLLER);
var CARCON, CARS, CONS,
	pesquisa = {
		selector:".carcon-list li",
		field:"label"
	}, $frmSearch, $txtSearch, $carconChk, $carconStatus;

$(document).ready(function(e){
	$frmSearch = $('.frm-carcon');
	$txtSearch = $('.txt',$frmSearch);
	$carconChk = $('.carcon-chk');
	$carconStatus = $('.carcon-status');

	$txtSearch.keyup(function(event) {
	    if (event.keyCode == 27) { //if press Esc
			$(this).val("").blur();
	    }
	    // console.log(this.value);
	    if(event.keyCode == 27 || $.trim(this.value) == ''){ //if press Esc or no text
	        $(pesquisa.selector).removeClass('visible').show().addClass('visible');
	    } else { //if there is text, lets filter
	        filter(pesquisa.selector,pesquisa.field,$(this).val());
	    }
	});

	$frmSearch.submit(function(e){
		e.preventDefault();
		$txtSearch.blur();
		return false;
	});

	$carconChk.change(function(e){
		var $this = $(this),
			car = $this.data('car'),
			con = $this.data('con');
		
		if(this.checked)
			Carcon.add(car,con);
		else
			Carcon.del(car,con);
	});
});


const Carcon = {
	url_add: URL+'/admin/concessionarias/carcon-add.json',
	url_del: URL+'/admin/concessionarias/carcon-del.json',

	add: function(car,con) {
		$.post(this.url_add,{'car':car,'con':con},function(json){
			if(json.error){
				alert(json.error);
				return;
			}
			Carcon.status(json.msg);
		},'json');
	},

	del: function(car,con) {
		$.post(this.url_del,{'car':car,'con':con},function(json){
			if(json.error){
				alert(json.error);
				return;
			}
			Carcon.status(json.msg);
		},'json');
	},

	status: function(msg) {
		// Messenger.add(msg).show(5000);
		$carconStatus.text(msg).show();
		_sto(function(){
			$carconStatus.fadeOut(function(){
				$carconStatus.text('');
			});
		},5.5);
	}
}

function filter(selector,field,query) {
	query =	$.trim(query);
	// query = query.replace(/ /gi, '|'); //add OR for regex
	query = query.replace(/ /gi, '.*'); //add OR for regex
	// console.log([selector,field,query]);
  
	$(selector).each(function() {
		(($(this).find(field).val() ? $(this).find(field).val().search(new RegExp(query, "i")) : $(this).find(field).text().search(new RegExp(query, "i"))) < 0) ?
			$(this).hide().removeClass('visible') :
			$(this).show().addClass('visible');
	});
}

