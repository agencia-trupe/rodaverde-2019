console.log(CONTROLLER);
var MARCON, MARS, CONS,
	pesquisa2 = {
		selector:".marcon-list li",
		field:"label"
	}, $frmSearch2, $txtSearch2, $marconChk, $marconStatus;

$(document).ready(function(e){
	$frmSearch2 = $('.frm-marcon');
	$txtSearch2 = $('.txt',$frmSearch2);
	$marconChk = $('.marcon-chk');
	$marconStatus = $('.marcon-status');

	$txtSearch2.keyup(function(event) {
	    if (event.keyCode == 27) { //if press Esc
			$(this).val("").blur();
	    }
	    // console.log(this.value);
	    if(event.keyCode == 27 || $.trim(this.value) == ''){ //if press Esc or no text
	        $(pesquisa2.selector).removeClass('visible').show().addClass('visible');
	    } else { //if there is text, lets filter2
	        filter2(pesquisa2.selector,pesquisa2.field,$(this).val());
	    }
	});

	$frmSearch2.submit(function(e){
		e.preventDefault();
		$txtSearch2.blur();
		return false;
	});

	$marconChk.change(function(e){
		var $this = $(this),
			mar = $this.data('mar'),
			con = $this.data('con');
		
		if(this.checked)
			Marcon.add(mar,con);
		else
			Marcon.del(mar,con);
	});
});


const Marcon = {
	url_add: URL+'/admin/concessionarias/marcon-add.json',
	url_del: URL+'/admin/concessionarias/marcon-del.json',

	add: function(mar,con) {
		$.post(this.url_add,{'mar':mar,'con':con},function(json){
			if(json.error){
				alert(json.error);
				return;
			}
			Marcon.status(json.msg);
		},'json');
	},

	del: function(mar,con) {
		$.post(this.url_del,{'mar':mar,'con':con},function(json){
			if(json.error){
				alert(json.error);
				return;
			}
			Marcon.status(json.msg);
		},'json');
	},

	status: function(msg) {
		// Messenger.add(msg).show(5000);
		$marconStatus.text(msg).show();
		_sto(function(){
			$marconStatus.fadeOut(function(){
				$marconStatus.text('');
			});
		},5.5);
	}
}

function filter2(selector,field,query) {
	query =	$.trim(query);
	// query = query.replace(/ /gi, '|'); //add OR for regex
	query = query.replace(/ /gi, '.*'); //add OR for regex
	// console.log([selector,field,query]);
  
	$(selector).each(function() {
		(($(this).find(field).val() ? $(this).find(field).val().search(new RegExp(query, "i")) : $(this).find(field).text().search(new RegExp(query, "i"))) < 0) ?
			$(this).hide().removeClass('visible') :
			$(this).show().addClass('visible');
	});
}

