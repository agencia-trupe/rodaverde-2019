<?php

class Application_Form_Contato extends ZendPlugin_Form
{
    public $mensagem_id;

    public function __construct($mid=null)
    {
        if($mid) $this->mensagem_id = $mid;
        return parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')
             ->setAction(URL.'/contato/enviar/')
             ->setAttrib('id','frm-fale-conosco')
             ->setAttrib('name','frm-fale-conosco');
        
        // elementos
        $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
        $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
        $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
        $this->addElement('text','assunto',array('label'=>'assunto','class'=>'txt2'));
        $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
        
        // filtros / validações
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('mensagem')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('email')->setRequired()
             ->addValidator('EmailAddress')
             ->addFilter('StripTags');
        
        // remove decoradores
        $this->removeDecs();
    }

    public function changeClasses($class='txt',$disable=false)
    {
        foreach ($this->getElements() as $elm)
            if($elm->getType()!='Zend_Form_Element_Radio'){
                $elm->setAttrib('class',$class);
                if($disable) $elm->setAttrib('disabled',true);
            }
        return $this;
    }

    public function setAdminParams()
    {
        if($this->getElement('tema')){
            $this->removeElement('tema');
            $this->addElement('text','tema',array('label'=>'tema','class'=>'txt2'));
        }

        if($this->getElement('cidade')){
            $this->removeElement('cidade');
            $this->addElement('text','cidade',array('label'=>'região','class'=>'txt2'));
        }

        if($this->getElement('assunto')){
            $this->removeElement('assunto');
            // $this->addElement('text','assunto',array('label'=>'assunto','class'=>'txt2'));
        }

        if($this->getElement('certificacao')){
            $this->removeElement('certificacao');
            $this->addElement('text','certificacao',array('label'=>'certificação','class'=>'txt2'));
        }

        if($this->getElement('horario')){
            $this->removeElement('horario');
            $this->addElement('text','horario',array('label'=>'horário','class'=>'txt2'));
        }

        if($this->getElement('participantes')){
            $this->getElement('participantes')->setLabel('n. de participantes');
        }

        if($this->mensagem_id==2){
            $this->removeElement('mensagem');
            $this->removeElement('assunto');
            $this->addElement('text','data',array('label'=>'data agendamento','class'=>'txt2 mask-date'));

            ($this->getElement('tema')) ?
                $this->getElement('tema')->setLabel('concessionária') :
                $this->addElement('text','tema',array('label'=>'concessionária','class'=>'txt2'));
            
        }

        if(in_array($this->mensagem_id, array(3,4,5))){
            // $this->removeElement('assunto');

            ($this->getElement('tema')) ?
                $this->getElement('tema')->setLabel('fornecedor') :
                $this->addElement('text','tema',array('label'=>'fornecedor','class'=>'txt2'));
            
        }

        $this->changeClasses('txt disabled',true);
    }
}

