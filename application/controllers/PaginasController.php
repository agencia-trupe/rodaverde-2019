<?php

class PaginasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->paginas = db_table('paginas');
    }

    public function indexAction()
    {
        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        $alias1 = ($this->_hasParam('alias1')) ? $this->_getParam('alias1') : null;

        if(!$alias) return $this->_redirect('/');
        $where_pag = $alias;

        $pag = null; $pag1 = null;
        if($alias1) {
            $pag1 = $this->paginas->fetchRow('alias="'.$alias1.'"');
            $where_pag = 'alias="'.$alias.'"';
            if($pag1) $where_pag.= ' and parent_id = "'.$pag1->id.'"';
            $pag = $this->paginas->fetchRow($where_pag);
        }
        // _d($pag);

        $pagina = $pag ? $this->paginas->getPagina($pag->id) : $this->paginas->getPagina($alias);
        $this->view->pagina = $pagina;
        // _d($pagina);

        $pagina1 = $this->paginas->getPagina($alias1);
        $this->view->pagina1 = $pagina1;
        // _d($pagina1);

        // metas e outras view configs
        $this->view->meta_description = (bool)trim($pagina->meta_description)?
            $pagina->meta_description :
            Is_Str::crop(
                $pagina->titulo.' '.
                Php_Html::toText($pagina->body)
            ,300);
        
        $this->view->meta_keywords = (bool)trim($pagina->meta_tags) ?
            $pagina->meta_tags :
            Is_Str::text2keywords(
                Php_Html::toText($this->view->meta_description),', ',10
            );

        // meta og
        $this->view->titulo = @$pagina1->titulo.' | '.$pagina->titulo;
        $this->view->meta_og_url = URL.'/'.$alias1.'/'.$alias;
        if(count($pagina->fotos)){
            $this->view->meta_og_image = IMG_URL.'/paginas/'.$pagina->fotos[0]->path;
        }
    }


}

