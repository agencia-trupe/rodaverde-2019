<?php

class VeiculosPesadosController extends ZendPlugin_Controller_Action
{
    protected $mensagem_id = 3;

    public function init()
    {
        // $this->paginas = new Application_Model_Db_Paginas();
        // $this->fornecedores = new Application_Model_Db_Fornecedores();
        $this->paginas = db_table('paginas');
        $this->fornecedores = db_table('fornecedores');
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(2);
        $this->view->pagina = $pagina;
        // _d($pagina);

        // metas e outras view configs
        $this->view->meta_description = (bool)trim($pagina->meta_description)?
            $pagina->meta_description :
            Is_Str::crop(
                $pagina->titulo.' '.
                Php_Html::toText($pagina->body)
            ,300);
        
        $this->view->meta_keywords = (bool)trim($pagina->meta_tags) ?
            $pagina->meta_tags :
            Is_Str::text2keywords(
                Php_Html::toText($this->view->meta_description),', ',10
            );
        // meta og
        $this->view->titulo = $pagina->titulo;
        $this->view->meta_og_url = URL.'/'.$pagina->alias;
        if((bool)trim($pagina->fotos_fixas->foto1)){
            $this->view->meta_og_image = IMG_URL.'/paginas/'.$pagina->fotos_fixas->foto1;
        }

        $forns = _utfRows($this->fornecedores->fetchAll(
        	'tipo = "vp" and status_id = 1',
        	'titulo'
        ));
        $this->view->forns = $forns;
        // _d($forns);
    }

    public function enviarAction()
    {
        $r = $this->getRequest();

        // if(!$this->isAjax() && !$r->isPost()){
        //     $this->messenger->addMessage('Requisição inválida','error');
        //     return $this->_redirect($this->view->controller);
        // }
        if(!$r->isPost()) exit(json_encode(array('error'=>'Requisição inválida')));

        // $this->mailling = new Application_Model_Db_Mailling();
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->mensagens_contato = new Application_Model_Db_MensagensContato();
        $this->mensagem = $this->mensagens->get($this->mensagem_id);
        
        // $form = new Application_Form_Contato();
        $post = $r->getPost();
        $assunto = (bool)trim($r->getParam('assunto')) ? ' - '.trim($r->getParam('assunto')) : $this->mensagem->subject;

        $tema = $this->mensagens->s1('fornecedores','*','id = "'.$post['tema_id'].'"');
        
        // if($form->isValid($post)){ // valida post
        if(true){ // valida post
            $html = "<h1>".$assunto."</h1>". // monta html
                    nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                    "<b>E-mail:</b> <a href='mailto:".
                    $r->getParam('email')."'>".$r->getParam('email').
                    "</a><br/>".
                    "<b>Telefone:</b> ".$r->getParam('telefone');
                    // "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>".
                    // "<b>Celular:</b> ".$r->getParam('celular');
            
            $html.= "<br/>";
            $html.= "<b>Fornecedor:</b> ".$r->getParam('tema')."<br/>";
            
            $data_mensagem_contato = array(
                'tema_id'  => $post['tema_id'],
                'tema'     => $post['tema'],
                'nome'     => $post['nome'],
                'email'    => $post['email'],
                'telefone' => Is_Cpf::clean($post['telefone']),
                'assunto'  => $assunto,
                'mensagem' => $post['mensagem'],
                // 'data'     => Is_Date::br2am($post['data']),
                'data_cad' => date('Y-m-d H:i:s'),
                'mensagem_id' => $this->mensagem_id,
            );

            try {
                // salva mensagem de contato
                $this->mensagens_contato->insert($data_mensagem_contato);

                $mail_params = (@$tema->email) ? 
                    array('to'=>$tema->email,'bcc'=>$this->mensagem->email):
                    array('to'=>$this->mensagem->email);

                // tenta enviar o e-mail
                if(ENV_PRO || 1) try { Trupe_Rodaverde_Mail::sendWithReply(
                    $post['email'],
                    $post['nome'],
                    ''.$assunto,
                    $html
                    ,null,null,$mail_params
                ); } catch(Exception $e){ }
                // return array('msg'=>'Mensagem enviada com sucesso!');
                $return = array('msg'=>$this->mensagem->body);
            } catch(Exception $e){
                $return = array('error'=>$e->getMessage());
            }
        } else {
            $return = array('error'=>'* Preencha todos os campos');
        }

        exit(json_encode($return));
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}

