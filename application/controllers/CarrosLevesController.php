<?php

class CarrosLevesController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace(SITE_NAME.'_comparativo');
        if(!@$this->session->carros) $this->session->carros = array();
        if($this->_hasParam('dump') && ENV_DEV) _d($this->session->carros);
        $this->view->sess_carros = $this->session->carros;
        $this->table = db_table('produtos');
    }

    public function indexAction()
    {
        $limit = 1000; $pages = 10;
        $busca = ($this->_hasParam('busca')) ? $this->_getParam('busca') : null;	
        if($busca) $busca = addslashes($this->_getParam('busca'));
        $where = 'tipo = 1 ';
        $order = 'id desc';
        // _d($this->_request->getParams());

        // filtros
        $filtro = ($this->_hasParam('filtro')) ? $this->_getParam('filtro') : null;
        $value = ($this->_hasParam('value')) ? $this->_getParam('value') : null;
        // _d(array($filtro=>$value));
        $p_marca = ($this->_hasParam('marca')) ? $this->_getParam('marca') : null;
        $p_modelo = ($this->_hasParam('modelo')) ? $this->_getParam('modelo') : null;
        $p_ano = ($this->_hasParam('ano')) ? $this->_getParam('ano') : null;
        $p_assentos = ($this->_hasParam('assentos')) ? $this->_getParam('assentos') : null;
        $p_adicionais = ($this->_hasParam('adicionais')) ? $this->_getParam('adicionais') : null;
        $p_fcharge = ($this->_hasParam('fcharge')) ? $this->_getParam('fcharge') : null;
        $p_sprint = ($this->_hasParam('sprint')) ? $this->_getParam('sprint') : null;
        // _d(array('marca'=>$p_marca,'ano'=>$p_ano,'modelo'=>$p_modelo,'assentos'=>$p_assentos,'adicionais'=>$p_adicionais,'fcharge'=>$p_fcharge,'sprint'=>$p_sprint));

        // testes Urio
        // $urip = new Urip($this->view->controller);
        // if($filtro){
        //     // $fparams = (object)$urip->get();
        //     // $fparams->ano = '2019';
        //     // $fparams->assentos = '4+6+1';
        //     // $uri = $urip->set((array)$fparams,$this->view->controller);
        //     $fparams = array('assentos'=>5,'ano'=>2019);
        //     $uri = $urip->replace($fparams);
        // } else {
        //     $fparams = array('marca'=>1);
        //     $uri = $urip->replace($fparams);
        // }
        // _d($uri);

        // if($filtro) switch($filtro) {
        //     case 'marca':
        //         $marca = $this->table->s1('marcas','id,alias,descricao','alias="'.$value.'"');
        //         if($marca) $where.= 'and marca_id = "'.$marca->id.'" ';
        //         break;
        // }
        $w_marca_in = null;
        if($filtro=='marca' || $p_marca){
            $w = 'alias in ("'.str_replace(' ','","',($p_marca) ? $p_marca : $value).'")';
            $marcas = $this->table->s('marcas','id,alias,descricao',$w);
            if($marcas){
                $marcas_id = array_map(function($m){
                    return $m->id;
                }, $marcas);
                $where.= 'and marca_id in ("'.implode('","', $marcas_id).'") ';
                $w_marca_in = 'and marca_id in ("'.implode('","', $marcas_id).'") ';
            }
        }

        if($filtro=='modelo' || $p_modelo){
            $mod_alias = ($p_modelo) ? $p_modelo : $value;
            $w_mod_in = str_replace(array(' ','-'),array('","',' '),$mod_alias);
            $where.= 'and modelo in ("'.$w_mod_in.'") ';
        }

        if($filtro=='ano' || $p_ano){
            $where.= 'and ano in ("'.str_replace(' ','","',($p_ano) ? $p_ano : $value).'") ';
        }

        if($filtro=='assentos' || $p_assentos){
            $where.= 'and assentos in ("'.str_replace(' ','","',($p_assentos) ? $p_assentos : $value).'") ';
        }

        if($filtro=='adicionais' || $p_adicionais){
            $adcns = explode(' ',$p_adicionais ? $p_adicionais : $value);
            $w_adcns_lks = array(); foreach($adcns as $adcn)
                $w_adcns_lks[] = 'adicionais like ("%,'.$adcn.',%")';
            if(count($w_adcns_lks))
                $where.= 'and ('.implode(' or ',$w_adcns_lks).') ';
        }

        if($filtro=='fcharge' || $p_fcharge){
            $where.= 'and fcharge in ("'.str_replace(' ','","',($p_fcharge) ? $p_fcharge : $value).'") ';
        }

        if($filtro=='sprint' || $p_sprint){
            // $where.= 'and car_sprint_0_100_el in ("'.str_replace(array(' ','_'),array('","',' '),($p_sprint) ? $p_sprint : $value).'") ';
            $w_sprint = array();
            foreach(explode(' ',(($p_sprint) ? $p_sprint : $value)) as $psv)
                $w_sprint[]= 'car_sprint_0_100_el like "'.$psv.'%"';
            $where.= 'and ('.implode(' or ',$w_sprint).')';
        }
        // _d($where);

        $raio = ($this->_hasParam('raio')) ? $this->_getParam('raio') : null;
        if($raio){
            $raios = explode('_', $raio);
            $raio_min = $raios[0];
            $raio_max = $raios[1];
            $where.= 'and autonomia_el >= "'.$raio_min.'" and autonomia_el <= "'.$raio_max.'" ';
            
            $this->view->raio_min = $raio_min;
            $this->view->raio_max = $raio_max;
        }
        
        $preco = ($this->_hasParam('preco')) ? $this->_getParam('preco') : null;
        if($preco){
            $precos = explode('_', $preco);
            $preco_min = $precos[0];
            $preco_max = $precos[1];
            $where.= 'and valor >= "'.$preco_min.'" and valor <= "'.$preco_max.'" ';
            
            $this->view->preco_min = $preco_min;
            $this->view->preco_max = $preco_max;
        }
        
        $pag = $this->pagination($limit,$pages);
        $rows = $busca ?
        	$this->table->search($busca,$limit,$pag->offset):
        	_utfRows($this->table->fetchAll($where,$order,$limit,$pag->offset));
        $this->paginationTotal($this->table->selectFoundRows());

        $this->view->rows = $rows;
        // _d($rows);

        // $marcas = $this->table->kv('marcas','descricao',0,'ordem','status_id=1');
        $marcas = $this->table->kva('marcas','descricao',0,'ordem','status_id=1');
        // $_marcas = $this->table->s('marcas','id,alias,descricao','status_id=1','ordem');
        // $marcas = array(); foreach($_marcas as $m) $marcas[$m->alias.'-'.$m->id] = $m->descricao;
        $this->view->marcas = $marcas;
        $this->view->searchMarca = $filtro=='marca' ? explode(' ',$value) : array();
        if($p_marca) $this->view->searchMarca = explode(' ',$p_marca);
        // _d($this->view->searchMarca,0); _d($marcas);

        $_modelos = $this->table->q('select modelo from produtos where status_id = 1 and tipo = 1 '.$w_marca_in.' group by modelo order by modelo');
        $modelos = array(); foreach($_modelos as $an) $modelos[$an->modelo] = $an->modelo; $modelos = array_filter($modelos);
        $this->view->modelos = $modelos;
        $this->view->searchModelo = $filtro=='modelo' ? explode(' ',$value) : array();
        if($p_modelo) $this->view->searchModelo = explode(' ',$p_modelo);
        // _d($this->view->searchModelo,0); _d($modelos);

        $_anos = $this->table->q('select ano from produtos where status_id = 1 and tipo = 1 group by ano order by ano');
        $anos = array(); foreach($_anos as $an) $anos[$an->ano] = $an->ano; $anos = array_filter($anos);
        $this->view->anos = $anos;
        $this->view->searchAno = $filtro=='ano' ? explode(' ',$value) : array();
        if($p_ano) $this->view->searchAno = explode(' ',$p_ano);
        // _d($this->view->searchAno,0); _d($anos);

        $_assentos = $this->table->q('select assentos from produtos where status_id = 1 and tipo = 1 group by assentos order by assentos');
        $assentos = array(); foreach($_assentos as $an) $assentos[$an->assentos] = $an->assentos; $assentos = array_filter($assentos);
        $this->view->assentos = $assentos;
        $this->view->searchAssentos = $filtro=='assentos' ? explode(' ',$value) : array();
        if($p_assentos) $this->view->searchAssentos = explode(' ',$p_assentos);
        // _d($this->view->searchAssentos,0); _d($assentos);

        $_adicionais = $this->table->q('select id,descricao from adicionais where status_id = 1 order by descricao');
        $adicionais = array(); foreach($_adicionais as $an) $adicionais[$an->id] = $an->descricao; $adicionais = array_filter($adicionais);
        $this->view->adicionais = $adicionais;
        $this->view->searchAdicionais = $filtro=='adicionais' ? explode(' ',$value) : array();
        if($p_adicionais) $this->view->searchAdicionais = explode(' ',$p_adicionais);
        // _d($this->view->searchAdicionais,0); _d($adicionais);

        // $_fcharges = $this->table->q('select fcharge from produtos where status_id = 1 and tipo = 1 '.$w_marca_in.' group by fcharge order by fcharge');
        $fcharges = array(1=>'Sim',0=>'Não'); //foreach($_fcharges as $an) $fcharges[$an->fcharge] = $an->fcharge; $fcharges = array_filter($fcharges);
        $this->view->fcharges = $fcharges;
        $this->view->searchFcharge = $filtro=='fcharge' ? explode(' ',$value) : array();
        if($p_fcharge!==null) $this->view->searchFcharge = explode(' ',$p_fcharge);
        // _d($this->view->searchFcharge,0); _d($fcharges);

        $_sprints = $this->table->q('select car_sprint_0_100_el from produtos where status_id = 1 and tipo = 1 '.$w_marca_in.' group by car_sprint_0_100_el order by cast(car_sprint_0_100_el as unsigned) asc');
        // $sprints = array(); foreach($_sprints as $an) $sprints[str_replace(' ', '_', $an->car_sprint_0_100_el)] = $an->car_sprint_0_100_el; $sprints = array_filter($sprints);
        $sprints = array(); foreach($_sprints as $an) if(trim($an->car_sprint_0_100_el)!='') $sprints[reset(explode(',',$an->car_sprint_0_100_el))] = reset(explode(',',$an->car_sprint_0_100_el)).' seg'; $sprints = array_filter($sprints);
        $this->view->sprints = $sprints;
        $this->view->searchSprint = $filtro=='sprint' ? explode(' ',$value) : array();
        if($p_sprint) $this->view->searchSprint = explode(' ',$p_sprint);
        // _d($this->view->searchSprint,0); _d($sprints);

        $this->view->countComparativo = count($this->session->carros);
        $this->view->action = 'index';
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        if(!$alias) return $this->_redirect('carros-leves');
        $alias = explode('-', $alias);
        $id = array_pop($alias);
        $alias = implode('-', $alias);
        // _d(array($id,$alias));

        $row = _utfRow($this->table->fetchRow(
            'id = "'.$id.'"'
        ));
        if(!$row) return $this->_forward('not-found','error');
    	if($row->alias!=$alias) return $this->_forward('not-found','error');
        $row = reset($this->table->getFotos(array($row),1));
        // _d($row);

        $this->view->row = $row;

        $configs = $this->table->s('produtoconfigs','*','status_id=1','ordem');
        $this->view->configs = $configs;
        // _d($configs);

        // metas e outras view configs
        $this->view->meta_description = (bool)trim($row->meta_description)?
            $row->meta_description :
            Is_Str::crop(
                $row->titulo.' '.
                Php_Html::toText($row->info)
            ,300);
        
        $this->view->meta_keywords = (bool)trim($row->meta_tags) ?
            $row->meta_tags :
            Is_Str::text2keywords(
                Php_Html::toText($this->view->meta_description),', ',10
            );

        // meta og
        $this->view->titulo = $row->titulo;
        $this->view->meta_og_url = URL.'/'.$this->view->controller.'/'.$row->alias.'-'.$row->id;
        if(count($row->fotos)){
            $this->view->meta_og_image = IMG_URL.'/produtos/'.$row->fotos[0]->path;
        }
        if((bool)trim($row->capa_path)){
            $this->view->meta_og_image = IMG_URL.'/produtos/'.$row->capa_path;
        }
    }


}

