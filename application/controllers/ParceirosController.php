<?php

class ParceirosController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        // $this->paginas = new Application_Model_Db_Paginas();
        // $this->fornecedores = new Application_Model_Db_Fornecedores();
        $this->fornecedores = db_table('fornecedores');
        $this->cons = db_table('concessionarias');
    }

    public function indexAction()
    {
        // $pagina = $this->paginas->getPagina(3);
        // $this->view->pagina = $pagina;
        // // _d($pagina);

        // pegando e separando por tipos/categorias
        $tipos = array_map('utf8_decode', $this->cons->kva('parceirotipos','descricao',0,'ordem','alias not in ("vp","cr","ps") '));
        // _d($tipos);
        $parcs = array();
        foreach($tipos as $tpK => $tpV){
            $parcs[$tpK] = (object)array(
                'categoria' => $tpV,
                'rows' => array(),
            );
        }
        
        $patrs=array(); $monts=array(); $paineis=array(); $carregs=array();
        $forns = _utfRows($this->fornecedores->fetchAll(
            'status_id = 1',
            'titulo'
        ));
        foreach($forns as $f) {
            if(!in_array($f->tipo, array("vp","cr","ps"))) 
                $parcs[$f->tipo]->rows[] = $f;

            switch ($f->tipo) {
                case 'pt': $patrs[] = $f; break;
                case 'mt': $monts[] = $f; break;
                case 'ps': $paineis[] = $f; break;
                case 'cr': $carregs[] = $f; break;
            }
        }
        // _d($parcs);
        // _d($patrs);
        // _d($monts);
        // _d($paineis);
        // _d($carregs);
        $this->view->parcs = $parcs;
        $this->view->patrs = $patrs;
        $this->view->monts = $monts;
        $this->view->paineis = $paineis;
        $this->view->carregs = $carregs;

        $cons = $this->cons->fetchAllWithFoto(
            'status_id = 1',
            'titulo'
        );
        $this->view->cons = $cons;
        // _d($cons);
    }

    public function index1Action()
    {
        // $pagina = $this->paginas->getPagina(3);
        // $this->view->pagina = $pagina;
        // // _d($pagina);
        
        $patrs=array(); $monts=array(); $paineis=array(); $carregs=array();
        $forns = _utfRows($this->fornecedores->fetchAll(
            'status_id = 1',
            'titulo'
        ));
        foreach($forns as $f) switch ($f->tipo) {
            case 'pt': $patrs[] = $f; break;
            case 'mt': $monts[] = $f; break;
            case 'ps': $paineis[] = $f; break;
            case 'cr': $carregs[] = $f; break;
        }
        // _d($patrs);
        // _d($monts);
        // _d($paineis);
        // _d($carregs);
        $this->view->patrs = $patrs;
        $this->view->monts = $monts;
        $this->view->paineis = $paineis;
        $this->view->carregs = $carregs;

        $cons = $this->cons->fetchAllWithFoto(
        	'status_id = 1',
        	'titulo'
        );
        $this->view->cons = $cons;
        // _d($cons);
    }


}

