<?php

class ComparativoController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->session = new Zend_Session_Namespace(SITE_NAME.'_comparativo');
        if(!@$this->session->carros) $this->session->carros = array();
        $this->table = db_table('produtos');
    }

    public function indexAction()
    {
        if(!(bool)@$this->session->carros) return $this->_redirect('carros-leves');
        $order = 'id desc';
        $where = 'tipo = 1 '.
                 'and id in ('.implode(',', $this->session->carros).') ';
        
        $rows = _utfRows($this->table->fetchAll($where,$order));
        if(!$rows) return $this->_redirect('carros-leves');
        // _d($rows);

        $this->view->rows = $rows;

        $configs = $this->table->s('produtoconfigs','*','status_id=1','ordem');
        $this->view->configs = $configs;
        // _d($configs);
    }

    public function addAction()
    {
        $id = ($this->_hasParam('carro')) ? (int)$this->_getParam('carro') : null;
        if(!$id) return array('error'=>'Veiculo invalido');

        $this->session->carros[] = $id;
        if(!ENV_DEV) $this->session->carros = array_unique($this->session->carros);
        return array('msg'=>'Veiculo adicionado ao comparativo');
    }
    
    public function delAction()
    {
        $id = ($this->_hasParam('carro')) ? (int)$this->_getParam('carro') : null;
        if(!$id) return array('error'=>'Veiculo invalido');

        $key = array_search($id, $this->session->carros);
        if($key !== false) unset($this->session->carros[$key]);
        return array('msg'=>'Veiculo removido do comparativo');
    }


}

