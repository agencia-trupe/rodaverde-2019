<?php

class AgendarTestDriveController extends ZendPlugin_Controller_Action
{
    protected $mensagem_id = 2;

    public function init()
    {
        $this->prod = db_table('produtos');
        $this->cons = db_table('concessionarias');

        $this->view->titulo = ($this->view->controller=='concessionarias') ?
            'Concessionárias':
            'Agendar Test Drive';
    }

    public function indexAction()
    {
        $pid = $this->_getParam('pid');
        if(in_array($pid, array('enviar','enviar.json'))) return $this->enviarAction();
        $carro = null;
        $where = array();
        $order = 'titulo';

        if($pid) {
        	$carro = _utfRow($this->prod->get($pid));
        	if(!$carro) return $this->_redirect('agendar-test-drive');
        	$this->view->carro = $carro;
        }
    	// _d($carro);

    	if($carro) {
        	$carcons = $this->cons->s('concessionarias_produtos','*','produto_id = "'.$carro->id.'"');
        	// _d($carcons);
        	if($carcons) {
	        	$carcon_ids = array_map(function($carcon){
	        		return $carcon->concessionaria_id;
	        	},$carcons);
        		// _d($carcon_ids);
                $where[]= 't1.id in ('.implode(',', $carcon_ids).')';
            } else {
	        	$where[]= 't1.id in (0)';
            }
    		// _d($where);
        }

        $marcas = $this->prod->kva('marcas','descricao',0,'descricao','status_id=1');
        $this->view->marcas = $marcas;
        // _d($marcas);

        $marca = $this->_getParam('marca');
        // _d($marca);
        if($marca) {
            $m = $this->cons->s1('marcas','id','alias = "'.$marca.'"');
            // _d($m);
            
            $mcids = array_map(function($mc){
                return $mc->concessionaria_id;
            },$this->cons->s('concessionarias_marcas','*','marca_id = "'.@$m->id.'"'));
            // _d($mcids);

            $where[]= 't1.id in ("'.implode('","', $mcids).'")';
            $this->view->marca = $marca;
        }
        // _d($where);

        $cep = $this->_getParam('cep');
        // _d($cep);
        if($cep) {
            $cep = Is_Cpf::clean($cep);
            // $where[]= 't1.cep = "'.$cep.'"';
            $this->view->cep = $cep;
        }
        // _d($this->view->cep);

        $where = count($where) ? implode(' and ', $where) : null;
        // _d($where);
        $rows = $this->cons->fetchAllWithFoto($where,$order);
        // _d($rows);

        // filtro por cep
        if($rows && $cep){
            $maxDistance = ENV_DEV ? 20000 : 50000;//100000;
            $latlngs = array_map(function($r){
                return $r->lat.','.$r->lng;
            }, $rows); // _d($latlngs);

            $geo = gmapsLatLng($cep); // _d($geo);
            $latlng = $geo ? $geo['lat'].','.$geo['lng'] : null; // _d($latlng);
            $dists  = $geo ? gmapsDistance($latlng,implode('|',$latlngs)) : null; // _d($dists);

            if($geo && $latlng && $dists){
                $latlngd = array();
                for($i=0;$i<sizeof($latlngs);$i++) $latlngd[] = array(
                    'latlng' => $latlngs[$i],
                    'lat' => reset(explode(',',$latlngs[$i])),
                    'lng' => end(explode(',',$latlngs[$i])),
                    'dist'=> $dists[$i]['distance']['value'],
                    'dist_text'=> $dists[$i]['distance']['text'].' ~ '.$dists[$i]['duration']['text'],
                );
                usort($latlngd,'cmpDist');
                // _d($latlngd);

                $_rows = array();
                foreach($rows as $r){
                    // $rlatlng = $r->lat.','.$r->lng; // _d($rlatlng,0);
                    $item = array_filter($latlngd, function($elm) use($r){
                        return $elm['latlng'] == $r->lat.','.$r->lng;
                    });
                    // _d($item,0);
                    if($item){
                        $item = reset($item);
                        $r->dist = @$item['dist'];
                        $r->dist_text = @$item['dist_text'];
                    }
                    if($r->dist < $maxDistance) $_rows[] = $r;
                }
                // _d($latlngd);
                usort($_rows,'cmpDistObj');
                $rows = $_rows;
            } else {
                $rows = array();
            }
            // _d($rows);
        }
        $this->view->rows = $rows;

        if($rows) { // pega lista de carros de cada cons
            $con_ids = array_map(function($r){
                return $r->id;
            }, $rows); // _d($con_ids);
            
            $_concars = $this->cons->s('concessionarias_produtos','*','concessionaria_id in ('.implode(',', $con_ids).')');
            $car_ids = array_map(function($r){
                return $r->produto_id;
            }, $_concars);

            $_cars = $this->cons->s('produtos','id,titulo','id in ('.(count($car_ids) ? implode(',',$car_ids) : '0').') and status_id = 1');
            $cars = array(); foreach ($_cars as $c) $cars[$c->id] = $c;
            
            $concars = array(); foreach ($_concars as $cc) {
                if(!array_key_exists($cc->concessionaria_id, $concars))
                    $concars[$cc->concessionaria_id] = array();
                $concars[$cc->concessionaria_id][] = $cars[$cc->produto_id];
            }
            $this->view->concars = $concars; // _d($concars);
        }
    }

    public function enviarAction()
    {
        $r = $this->getRequest();

        // if(!$this->isAjax() && !$r->isPost()){
        //     $this->messenger->addMessage('Requisição inválida','error');
        //     return $this->_redirect($this->view->controller);
        // }
        if(!$r->isPost()) exit(json_encode(array('error'=>'Requisição inválida')));

        // $this->mailling = new Application_Model_Db_Mailling();
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->mensagens_contato = new Application_Model_Db_MensagensContato();
        $this->mensagem = $this->mensagens->get($this->mensagem_id);
        
        // $form = new Application_Form_Contato();
        $post = $r->getPost();
        $assunto = (bool)trim($r->getParam('assunto')) ? ' - '.trim($r->getParam('assunto')) : $this->mensagem->subject;

        $tema = $this->mensagens->s1('concessionarias','*','id = "'.$post['tema_id'].'"');

        // validações
        $err = null;
        if(Is_Date::br2am($post['data']) < date('Y-m-d')) $err = '* Data inválida';
        if($err) exit(json_encode(array('error'=>$err)));
        
        // if($form->isValid($post)){ // valida post
        if(true){ // valida post
            $html = "<h1>".$assunto."</h1>". // monta html
                    // nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                    "<b>E-mail:</b> <a href='mailto:".
                    $r->getParam('email')."'>".$r->getParam('email').
                    "</a><br/>".
                    "<b>Telefone:</b> ".$r->getParam('telefone');
                    // "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>".
                    // "<b>Celular:</b> ".$r->getParam('celular');
            
            $html.= "<br/>";
            $html.= "<b>Data preferida para agendamento:</b> ".$r->getParam('data')."<br/>";
            $html.= "<br/>";
            $html.= "<b>Concessionária:</b> ".$r->getParam('tema')."<br/>";
            if((bool)trim(@$post['pids'])) {
                $pids = explode(',',substr($post['pids'],1,-1));
                $pids_rows = $this->mensagens->s('produtos','id,titulo','id in ("'.implode('","',$pids).'")');
                if($pids_rows){
                    $html.= "<b>Veículos:</b><br/>";
                    foreach($pids_rows as $pr) $html.= $pr->titulo."<br/>";
                    $html.= "<br/>";
                }
            }
            
            $data_mensagem_contato = array(
                'tema_id'  => $post['tema_id'],
                'tema2_ids'=> $post['pids'],
                'tema'     => $post['tema'],
                'nome'     => $post['nome'],
                'email'    => $post['email'],
                'telefone' => Is_Cpf::clean($post['telefone']),
                'assunto'  => $assunto,
                // 'mensagem' => $post['mensagem'],
                'data'     => Is_Date::br2am($post['data']),
                'data_cad' => date('Y-m-d H:i:s'),
                'mensagem_id' => $this->mensagem_id,
            );

            try {
                // salva mensagem de contato
                $this->mensagens_contato->insert($data_mensagem_contato);

                $mail_params = (@$tema->email) ? 
                    array('to'=>$tema->email,'bcc'=>$this->mensagem->email):
                    array('to'=>$this->mensagem->email);

                // tenta enviar o e-mail
                if(ENV_PRO || 1) try { Trupe_Rodaverde_Mail::sendWithReply(
                    $post['email'],
                    $post['nome'],
                    ''.$assunto,
                    $html
                    ,null,null,$mail_params
                ); } catch(Exception $e){ }
                // return array('msg'=>'Mensagem enviada com sucesso!');
                $return = array('msg'=>$this->mensagem->body);
            } catch(Exception $e){
                $return = array('error'=>$e->getMessage());
            }
        } else {
            $return = array('error'=>'* Preencha todos os campos');
        }

        exit(json_encode($return));
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}

