<?php

class IndexController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
        $this->noticias = new Application_Model_Db_BlogsPosts();
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(1);
        $this->view->pagina = $pagina;
        // _d($pagina);
        
        $bannerRandom = true;
        $bannerLimit = ENV_DEV ? 2 : 2;
        $banners = $pagina->fotos;
        // array_pop($banners);
        if($bannerRandom) shuffle($banners);
        if($bannerLimit) $banners = array_slice($banners, 0, $bannerLimit);
        $this->view->banners = $banners;
        // _d($banners);

        $chamadas = $this->paginas->s('homelinks','*','status_id=1','ordem');
        $this->view->chamadas = $chamadas;
        // _d($chamadas);
        
        $noticia = _utfRow(
            $this->noticias->fetchRow('status_id=1 and blog_id=2',array('destaque desc','data_edit desc'))
        );
        if($noticia) $noticia->fotos = $this->noticias->getFotos($noticia);
        $this->view->noticia = $noticia;
        // _d($noticia);
    }

    public function newsletterAction()
    {
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            // $post = $post['newsletter'];
            $post = $post;
            $validator = new Zend_Validate_EmailAddress();
            
            // if(trim(@$post['nome']) == '' || !$validator->isValid(@$post['email'])){
            if(!$validator->isValid(@$post['email'])){
                return array("error"=>1,"message"=>"* Preencha os campos corretamente","data"=>$post);
            } else {
                try {
                    // $data = $post;
                    $data = array_map('utf8_decode',$post);
                    $data['data_cad'] = date('Y-m-d H:i:s');
                    $data['nome'] = reset(explode('@',$data['email']));
                    //unset($data['submit']);
                    $table = new Application_Model_Db_Mailling();
                    
                    $html = '<h1 style="font-size:14px">Novo cadastro em Newsletter</h1><p style="font-size:11px">'.
                        // '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '</p>';
                    
                    try { Trupe_Rodaverde_Mail::sendWithReply(
                        $data['email'],
                        $data['nome'],
                        // $post['nome'],
                        'Novo cadastro em Newsletter',
                        $html
                    ); } catch(Exception $e){ }
                    
                    $html2= '<p style="font-size:11px">'.
                        'Cadastro em Newsletter realizado com sucesso!<br /><br />'.
                        // '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '</p>';
                    
                    try { Trupe_Rodaverde_Mail::send(
                        $data['email'],
                        $data['nome'],
                        // $post['nome'],
                        'Confirmação de cadastro em Newsletter',
                        $html2
                    ); } catch(Exception $e){ }
                    
                    $table->insert($data);
                    return array("message"=>"Cadastro efetuado com sucesso!");
                    $form->reset();
                } catch(Exception $e){
                    // if(strstr($e->getMessage(),'uplicate')) return array('message'=>'* Você já está cadastrado.');
                    if(strstr($e->getMessage(),'uplicate')) return array('message'=>'Cadastro efetuado com sucesso!');
                    return array("error"=>1,"message"=>'* Erro ao enviar formulário');
                }
            }
        }
    }

    public function gmapsAction()
    {
        $cep = ($this->_hasParam('cep')) ? $this->_getParam('cep') : '04182010';
        $cep = urldecode($cep);
        $geocode = gmapsGeocode($cep);
        _d($geocode);
    }

    public function mailAction()
    {
        try{
            Trupe_Rodaverde_Mail::send('patrick.trupe@gmail.com','Trupe','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }


}