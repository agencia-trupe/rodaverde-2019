<?php
/**
 * Formatação de Datas
 * Auxiliar da Camada de Visualização
 * @author Wanderson Henrique Camargo Rosa
 * @see APPLICATION_PATH/views/helpers/Date.php
 * 
 * @see library/Zend/Date.php - consts
 * DAY               = 'dd'
 * DAY_SHORT         = 'd'
 * DAY_SUFFIX        = 'SS'
 * DAY_OF_YEAR       = 'D'
 * WEEKDAY           = 'EEEE'
 * WEEKDAY_SHORT     = 'EEE'
 * WEEKDAY_NARROW    = 'E'
 * WEEKDAY_NAME      = 'EE'
 * WEEKDAY_8601      = 'eee'
 * WEEKDAY_DIGIT     = 'e'
 * WEEK              = 'ww'
 * MONTH             = 'MM'
 * MONTH_SHORT       = 'M'
 * MONTH_DAYS        = 'ddd'
 * MONTH_NAME        = 'MMMM'
 * MONTH_NAME_SHORT  = 'MMM'
 * MONTH_NAME_NARROW = 'MMMMM'
 * YEAR              = 'y'
 * YEAR_SHORT        = 'yy'
 * YEAR_8601         = 'Y'
 * YEAR_SHORT_8601   = 'YY'
 * LEAPYEAR          = 'l'
 * MERIDIEM          = 'a'
 * SWATCH            = 'B'
 * HOUR              = 'HH'
 * HOUR_SHORT        = 'H'
 * HOUR_AM           = 'hh'
 * HOUR_SHORT_AM     = 'h'
 * MINUTE            = 'mm'
 * MINUTE_SHORT      = 'm'
 * SECOND            = 'ss'
 * SECOND_SHORT      = 's'
 * MILLISECOND       = 'S'
 * TIMEZONE_NAME     = 'zzzz'
 * DAYLIGHT          = 'I'
 * GMT_DIFF          = 'Z'
 * GMT_DIFF_SEP      = 'ZZZZ'
 * TIMEZONE          = 'z'
 * TIMEZONE_SECS     = 'X'
 * ISO_8601          = 'c'
 * RFC_2822          = 'r'
 * TIMESTAMP         = 'U'
 * ERA               = 'G'
 * ERA_NAME          = 'GGGG'
 * ERA_NARROW        = 'GGGGG'
 * DATES             = 'F'
 * DATE_FULL         = 'FFFFF'
 * DATE_LONG         = 'FFFF'
 * DATE_MEDIUM       = 'FFF'
 * DATE_SHORT        = 'FF'
 * TIMES             = 'WW'
 * TIME_FULL         = 'TTTTT'
 * TIME_LONG         = 'TTTT'
 * TIME_MEDIUM       = 'TTT'
 * TIME_SHORT        = 'TT'
 * DATETIME          = 'K'
 * DATETIME_FULL     = 'KKKKK'
 * DATETIME_LONG     = 'KKKK'
 * DATETIME_MEDIUM   = 'KKK'
 * DATETIME_SHORT    = 'KK'
 * ATOM              = 'OOO'
 * COOKIE            = 'CCC'
 * RFC_822           = 'R'
 * RFC_850           = 'RR'
 * RFC_1036          = 'RRR'
 * RFC_1123          = 'RRRR'
 * RFC_3339          = 'RRRRR'
 * RSS               = 'SSS'
 * W3C               = 'WWW'
 * 
 */
class Zend_View_Helper_Date2 extends Zend_View_Helper_Abstract
{
    /**
     * Manipulador de Datas
     * @var Zend_Date
     */
    protected static $_date = null;
    protected static $_locale = null;
 
    /**
     * Método Principal
     * @param string $value Valor para Formatação
     * @param string $format Formato de Saída
     * @return string Valor Formatado
     */
    public function date2($value, $format = 'DATETIME_MEDIUM')
    {
        // if($this->_locale=='en') $value = Is_Date::am2en($value);
        $format = eval("return Zend_Date::$format;");
        $date = $this->getDate();
        return $date->set(strtotime($value))->get($format,$this->_locale);
    }
 
    /**
     * Acesso ao Manipulador de Datas
     * @return Zend_Date
     */
    public function getDate()
    {
        if (self::$_date == null) {
            // $locale = $this->_locale = new Zend_Locale(Zend_Registry::get('Zend_Locale'));
            // self::$_date = new Zend_Date(null,Zend_Locale_Format::getDateFormat($locale),$locale);
            self::$_date = new Zend_Date();
        }
        return self::$_date;
    }
}