<?php
/**
 * Tradução de url - adiciona parâmetro de linguagem ao link
 * Auxiliar da Camada de Visualização
 * @author Patrick M. de Olveira - Trupe Agência Criativa
 * @see APPLICATION_PATH/views/helpers/TranslateDb.php
 */

class Zend_View_Helper_TranslateUrl extends Zend_View_Helper_Abstract
{
    /**
     * Método Principal
     *
     * @param string $url - url a ser traduzida
     *
     * @return string - Url traduzido
     */
    public function translateUrl($url)
    {
        if($this->view->lang != DEFAULT_LANGUAGE && !strstr($url,'lang=')){
            $simbol = strstr($url,'?') ? '&' : '?';
            $url.= $simbol.'lang='.$this->view->lang;
        }

        return $url;
    }
}