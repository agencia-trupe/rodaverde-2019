<?php

class Admin_Form_EsqueciMinhaSenha extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post')->setAction('')->setAttrib('id','frm-senha')->setAttrib('name','frm-senha');
		
		$this->addElement('text','email',array('label'=>'* Email:','class'=>'txt','validator'=>'EmailAddress'));        
		$this->addElement('submit','submit',array('label'=>'Enviar','class'=>'bt'));
        
        $this->getElement('email')->setRequired();
        
        $this->removeDecs(array('label','htmlTag','description','errors'));
    }
    
    public function removeDecs($decorators = array('label','htmlTag','description','errors'),$elms=array())
    {
        $_elms = &$this->getElements();
        $elms = count($elms) ? $elms : $_elms;
        foreach($elms as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
        return $this;
    }
}

