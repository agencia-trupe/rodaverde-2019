<?php

class Admin_Form_BlogsPosts extends ZendPlugin_Form
{
    public $blog_id = null;
    
    public function __construct($blog_id=null)
    {
        if($blog_id) $this->blog_id = $blog_id;
        return parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/blogs-posts/save/')
             ->setAttrib('id','frm-noticias')
             ->setAttrib('name','frm-noticias');
        
        $categs = new Application_Model_Db_CategoriasBlog();
        $categsKV = $categs->getKeyValues('descricao_pt',0,$this->blog_id);

        // elementos
        $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categsKV));
        $this->addElement('text','titulo_pt',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        // $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        $this->addElement('hidden','alias');
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','olho_pt',array('label'=>'Prévia (pt)','class'=>'txt'));
        // $this->addElement('textarea','olho_en',array('label'=>'Prévia (en)','class'=>'txt'));
        $this->addElement('textarea','body_pt',array('label'=>'Conteúdo','class'=>'txt wysiwyg'));
        $this->addElement('textarea','body_pt',array('label'=>'Conteúdo (pt)','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','body_en',array('label'=>'Conteúdo (en)','class'=>'txt wysiwyg'));
        $this->addElement('textarea','meta_description',array('label'=>'Meta Description:','class'=>'txt'));
        $this->addElement('textarea','meta_tags',array('label'=>'Meta Tags:','class'=>'txt'));
        
        $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('olho_pt')->setAttrib('rows',5)->setAttrib('cols',1);
        // $this->getElement('olho_en')->setAttrib('rows',5)->setAttrib('cols',1);
        $this->getElement('body_pt')->setAttrib('rows',15)->setAttrib('cols',1);
        // $this->getElement('body_en')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

