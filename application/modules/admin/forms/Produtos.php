<?php

class Admin_Form_Produtos extends ZendPlugin_Form
{
    public function init()
    {
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/produtos/save')
             ->setAttrib('id','frm-produtos')
             ->setAttrib('name','frm-produtos');
		
        $prods = db_table('produtos');
        // $generos = $prods->getGeneros();
        // $roles = $prods->getRoles();
        $otp_sn = array('0'=>'Não','1'=>'Sim');
        $marcas = $prods->kv('marcas','descricao',1);

        // elementos fixos
        $this->addElement('hidden','tipo');
        $this->addElement('text','titulo');
        $this->addElement('select','marca_id',array('label'=>'marca','class'=>'txt','multiOptions'=>$marcas));
        $this->addElement('text','modelo',array('label'=>'modelo','class'=>'txt'));
        $this->addElement('text','ano',array('label'=>'ano','class'=>'txt mask-int','maxlength'=>4));
        $this->addElement('text','assentos',array('label'=>'assentos','class'=>'txt mask-int','maxlength'=>2));
        $this->addElement('text','valor');
        $this->addElement('text','autonomia_el');
        $this->addElement('select','car_el',array('label'=>'totalmente elétrico','class'=>'txt','multiOptions'=>$otp_sn));
        $this->addElement('select','fcharge');
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));

        // elementos configs
        $configs_n = array('valor');
        $configs = $prods->s('produtoconfigs',
            'descricao,alias',
            null, // 'alias not in ("'.implode('","', $configs_n).'")',
            'ordem');
        $cfg_default = (object)array(
            'tipo'=>'text',
            'label'=>'',
            'class'=>'txt');
        $cfg_pers = array(
            'titulo' => array('label'=>'título (fabricante e modelo)'),
            'autonomia_el' => array('class' => 'txt mask-int'),
            'autonomia_el2' => array('class' => 'txt mask-int'),
            'alcance_inv' => array('class' => 'txt mask-int'),
            'alcance_ver' => array('class' => 'txt mask-int'),
            'car_veloc_max_el' => array('class' => 'txt mask-int'),
            'valor' => array(
                'label'=>'preço R$',
                'class' => 'txt mask-currency',
            ),
            'fcharge' => array(
                'tipo' => 'select',
                'opts' => array('multiOptions'=>$otp_sn),
            ),
            'disp_merc' => array(
                'tipo' => 'select',
                'opts' => array('multiOptions'=>$otp_sn),
            ),
            'op_fcharge' => array(
                'tipo' => 'select',
                'opts' => array('multiOptions'=>$otp_sn),
            ));
        foreach ($configs as $cfg) {
            $c = clone($cfg_default);
            $c->label = $cfg->descricao;

            $c2 = @$cfg_pers[$cfg->alias];
            if($c2) foreach($c2 as $k => $v) $c->{$k} = $v;

            $opts = array('label'=>$c->label,'class'=>$c->class);
            if(@$cfg_pers[$cfg->alias]['opts']) $opts = $cfg_pers[$cfg->alias]['opts']+$opts;
            
            $this->addElement($c->tipo,$cfg->alias,$opts);
        }
        // _d($configs);
        
        // elementos antigos
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$prods->kv('categorias','descricao',1)));
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getParentsKV($categs->getWithChildren(),array('__none__'=>'Selecione...'),true)));
		// $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','cod',array('label'=>'Código','class'=>'txt'));
        //$this->addElement('text','peso',array('label'=>'Peso','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','valor',array('label'=>'Valor R$','class'=>'txt txt3 mask-currency'));
        // //$this->addElement('text','valor_promo',array('label'=>'Valor Promo. R$','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','obs_valor',array('label'=>'Obs. Valor','class'=>'txt txt2'));
        // $this->addElement('text','medida_c',array('label'=>'Comp.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','medida_l',array('label'=>'Larg.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','medida_a',array('label'=>'Alt.','class'=>'txt txt3 mask-currency'));
		// $this->addElement('text','estoque',array('label'=>'Estoque','class'=>'txt txt3 mask-int'));
		// $this->addElement('text','estoque_minimo',array('label'=>'Estoque mín.','class'=>'txt txt3 mask-int'));
        // $this->addElement('radio','genero',array('label'=>'Gênero','class'=>'radio','multiOptions'=>$generos,'separator'=>' '));
        //$this->addElement('select','genero',array('label'=>'Gênero','class'=>'txt','multiOptions'=>array(0=>'Feminino',1=>'Masculino')));
        // $this->addElement('textarea','descricao',array('label'=>'Especificações','class'=>'txt wysiwyg','maxlength'=>'255'));
        // $this->addElement('textarea','descricao_completa',array('label'=>'Detalhes','class'=>'txt wysiwyg'));
        $this->addElement('textarea','info',array('label'=>'Observações:','class'=>'txt wysiwyg'));
        $this->addElement('textarea','meta_description',array('label'=>'Meta Description:','class'=>'txt'));
        $this->addElement('textarea','meta_tags',array('label'=>'Meta Tags:','class'=>'txt'));
        
        // $this->addElement('select','role',array('label'=>'Visível para','class'=>'txt','multiOptions'=>$roles));
        // $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        // $this->addElement('checkbox','novo',array('label'=>'Novidade'));
        
        // atributos
        // $this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        // $this->getElement('descricao_completa')->setAttrib('rows',1)->setAttrib('cols',1);
        $this->getElement('info')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('categoria_id')->setRequired();
        $this->getElement('titulo')->setRequired();
        // $this->getElement('cod')->setRequired();
        // $this->getElement('genero')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

