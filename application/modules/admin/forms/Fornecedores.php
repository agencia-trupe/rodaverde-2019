<?php

class Admin_Form_Fornecedores extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/fornecedores/save/')
             ->setAttrib('id','frm-fornecedores')
             ->setAttrib('name','frm-fornecedores');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Nome','class'=>'txt'));
        // $this->addElement('hidden','alias');
        $this->addElement('text','email',array('label'=>'E-mail','class'=>'txt'));
        // $this->addElement('text','endereco',array('label'=>'Endereço','class'=>'txt'));
        // $this->addElement('text','bairro',array('label'=>'Bairro','class'=>'txt'));
        // $this->addElement('text','cidade',array('label'=>'Cidade','class'=>'txt'));
        // $this->addElement('text','uf',array('label'=>'Estado','class'=>'txt','maxlength'=>2));
        // $this->addElement('text','cep',array('label'=>'CEP','class'=>'txt mask-cep'));
        // $this->addElement('text','tel',array('label'=>'Telefone','class'=>'txt mask-cel'));
        $this->addElement('text','site',array('label'=>'Site','class'=>'txt'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','body',array('label'=>'Informações','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','body2',array('label'=>'Texto oculto','class'=>'txt'));
        // $this->addElement('textarea','depoimento',array('label'=>'Depoimento','class'=>'txt'));
        $this->addElement('hidden','tipo');
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

