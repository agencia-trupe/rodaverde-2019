<?php

class Admin_Form_Homelinks extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/homelinks/save/')
             ->setAttrib('id','frm-homelinks')
             ->setAttrib('name','frm-homelinks');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        $this->addElement('text','url',array('label'=>'Link','class'=>'txt'));
        $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        // $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

