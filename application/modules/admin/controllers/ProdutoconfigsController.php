<?php

class Admin_ProdutoconfigsController extends ZendPlugin_Controller_Ajax
{
    protected $edit_mode = 0;
    
    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "Exibição de items";
        $this->view->section = $this->section = "produtoconfigs";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));
        $edit_mode = ($this->_hasParam('editmode')) ? $this->_getParam('editmode') : $this->edit_mode;
        $this->view->edit_mode = $edit_mode;

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));

        // models
        $this->produtoconfigs = new Application_Model_Db_Produtoconfigs();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        Admin_Model_Login::setControllerPermissions($this,$this->section);
    }
    
    public function indexAction()
    {
        $_produtoconfigs = $this->produtoconfigs->fetchAll(null,'ordem');
        
        if(count($_produtoconfigs)){
            $produtoconfigs = Is_Array::utf8DbResult($_produtoconfigs);
            
            for($i=0;$i<sizeof($produtoconfigs);$i++){
                $produtoconfigs[$i]->foto = $produtoconfigs[$i]->foto_id ?
                    $_produtoconfigs[$i]->findDependentRowset('Application_Model_Db_Fotos')->current() :
                    null;
                
                $children = $this->produtoconfigs->getChildren($produtoconfigs[$i]->id);
                $produtoconfigs[$i]->is_parent = count($children);
                $produtoconfigs[$i]->children = $children;
            }
        } else {
            $produtoconfigs = null;
        }
        
        $this->view->produtoconfigs = $produtoconfigs;
        
        $produtoconfigs_pai = $this->produtoconfigs->getParents($produtoconfigs,true);
        // key/values das produtoconfigs pai p/ montagem dos combos
        $produtoconfigs_pai_kv = $this->produtoconfigs->getParentsKV($produtoconfigs_pai,array('__none__'=>'Selecione...'));
        $this->view->produtoconfigs_pai = $produtoconfigs_pai;
        $this->view->produtoconfigs_pai_kv = $produtoconfigs_pai_kv;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $duplicate_count = 0;
        $duplicates = array();
        $params = $this->_request->getParams();
        
        try {
            for($i=0;$i<sizeof($params['id']);$i++){
                $data = array();
                $row = $this->produtoconfigs->fetchRow('id='.$params['id'][$i]); // verifica registro para atualização
                
                $data['descricao']    = (APPLICATION_ENV!='development1') ? utf8_decode($params['descricao'][$i]) : $params['descricao'][$i];
                // $data['body']         = (APPLICATION_ENV!='development1') ? utf8_decode($params['body'][$i]) : $params['body'][$i];
                // $data['desconto']     = (int)$params['desconto'][$i];
                $data['ordem']        = $params['ordem'][$i];
                $data['status_id']    = $params['status_id'][$i];
                // $data['produtoconfig_id'] = $params['produtoconfig_id'][$i] == '__none__' ? null : $params['produtoconfig_id'][$i];
                // $data['foto_id']      = $params['foto_id'][$i] == 0 ? null : $params['foto_id'][$i];
                // $data['alias']        = Is_Str::toUrl($params['descricao'][$i]);
                $data['alias']        = ((bool)trim($params['alias'][$i])) ? $params['alias'][$i] : null;
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                
                if($row){
                    $up = 0;
                    // if($row->alias != $data['alias']){
                        // if($this->produtoconfigs->fetchRow('alias="'.$data['alias'].'"')){
                            // $duplicates[] = utf8_encode('&rarr; <b>'.$row->descricao.'</b> alterado para <b>'.$data['descricao'].'</b>');
                            // $duplicate_count++;
                        // } else {
                            $row->descricao = $data['descricao'];
                            $row->alias     = $data['alias'];
                            $up++;
                        // }
                    // }
                    if($row->body != $data['body']){ $row->body = $data['body']; $up++; }
                    if($row->desconto != $data['desconto']){ $row->desconto = (bool)$data['desconto']?$data['desconto']:null; $up++; }
                    if($row->ordem != $data['ordem']){ $row->ordem = $data['ordem']; $up++; }
                    if($row->status_id != $data['status_id']){ $row->status_id = $data['status_id']; $up++; }
                    if($row->foto_id != $data['foto_id']){ $row->foto_id = $data['foto_id']; $up++; }
                    
                    if($row->produtoconfig_id != $data['produtoconfig_id']){
                        if(!count($this->produtoconfigs->getChildren($row->id))) { $row->produtoconfig_id = $data['produtoconfig_id']; $up++; }
                    }
                    
                    if($up > 0){
                        $row->data_edit = $data['data_edit'];
                        $row->save();
                    }
                } else {
                    if($this->produtoconfigs->fetchRow('alias="'.$data['alias'].'"')){
                        $duplicates[] = "&rarr; ".utf8_encode($data['descricao']);
                        $duplicate_count++;
                    } else {
                        $this->produtoconfigs->insert($data);
                    }
                }
            }
            
            // se há registros duplicados, adiciona mensagem
            ($duplicate_count > 0) ?
                $this->messenger->addMessage($duplicate_count.' registros possuem duplicidade. Por favor, altere-os e salve novamente:<br/>'.implode('<br/>',$duplicates),'error') :
                $this->messenger->addMessage('Registros atualizados.');
            
            $this->_redirect('admin/'.$this->section.'/');
            //$this->_forward('index');
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
        }
    }
    
    public function delAction()
    {
        $id = (int)$this->_getParam("id");
        
        try {
            $cat = $this->produtoconfigs->fetchRow('id='.$id);
            
            if($cat->foto_id) $this->fotoDel($cat->foto_id);
            
            $this->produtoconfigs->delete("id=".$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->produto_id)){
            $select->where('f2.produto_id = ?',$this->produto_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        return $this->fotoDel($id);
    }
    
    public function fotoDel($id)
    {
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
        
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('file'=>$_FILES);
            return $upload->getErrors();
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $cat_fotos = $this->produtoconfigs;
            $cat_id = $this->_hasParam('id') ? $this->_getParam('id') : null;
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)) return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            
            if($cat_id){
                $cat = $cat_fotos->fetchRow('id='.$cat_id);
                
                if($cat->foto_id) $this->fotoDel($cat->foto_id);
                
                $cat->foto_id = $foto_id;
                $cat->save();
            }
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}

