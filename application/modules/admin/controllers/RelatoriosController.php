<?php

class Admin_RelatoriosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
    	Application_Model_Login::checkAuth($this);
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        $this->relatorio = new Application_Model_Relatorio();
        $this->usuarios = new Application_Model_Db_Usuario();
        
        $this->view->titulo = "RELATÓRIOS";
        $this->view->section = $this->section = "relatorios";

        $this->relatorio_tipo =
        $relatorio_tipo = $this->_hasParam('relatorio_tipo') ?
                          $this->_getParam('relatorio_tipo') :
                          $this->relatorio->relatorio_padrao;
        // _d($this->relatorio_tipo);

        $this->tipos_ads = array(
            ''=>'Área:',
            '0'=>'Home',
            '1'=>'Internas');

        // if(APPLICATION_ENV=='production') $this->_redirect('admin');
    }

    public function indexAction()
    {
        //Admin_Model_Login::checkAuth($this,'relatorios') ||
            //$this->_forward('denied','error','default',array('url'=>URL.'/painel/login'));
        
        $this->relatorio_tipo =
        $relatorio_tipo = $this->_hasParam('relatorio_tipo') ?
                          $this->_getParam('relatorio_tipo') :
                          $this->relatorio->relatorio_padrao;
        
        // $this->view->titulo.= ' - '.$this->relatorio->getTipo($relatorio_tipo);
        $this->view->titulo = 'Relatório '.$this->relatorio->getTipo($relatorio_tipo);

        $params = $this->_request->getParams();

        switch ($relatorio_tipo) {
            case '1':
            case '2':
            case '3':
                $this->view->usuarios = $this->usuarios;
                break;
            
            case '5':
                $this->view->tipos = $this->tipos_ads;
                break;
            
            default:
                break;
        }

        $this->view->relatorio_tipos = $this->relatorio->relatorio_tipos;
        $this->view->relatorio_tipo = $relatorio_tipo;
        $this->view->params = $params;
        $this->view->rows = $this->relatorio->getReport($relatorio_tipo,$params);
        // _d($this->view->rows);

        $msgs = new Application_Model_Db_Mensagens();
        $mensagens = $msgs->getKeyValues('titulo',false,'subject','id not in (1)');
        $this->view->mensagens = array(''=>'Área:')+$mensagens;
        // _d($mensagens);

        return $this->view->rows;
    }

    public function exportAction()
    {
        $this->indexAction();
        $rows = (array)$this->view->rows;
        $params = $this->_request->getParams();
        $this->filename = Is_Str::toUrl('Relatório - '.$this->relatorio->getTipo($this->relatorio_tipo).' - '.SITE_TITLE);
        // _d($params);

        foreach($rows as &$row){
            // retirando params desnecessarios
            $row = (array)$row;

            $unsets = array(
                'id',
                'area_id',
                'area_Id',
                'tema_id',
                'clicks',
            	'ip',
            	// 'data_cad',
                'nome',
                'url',
                'session_id',
                'user_id'
            );

            switch($this->relatorio_tipo){
                case '1':
                    if((bool)@$params['filter']) unset($unsets[array_search($params['filter'],$unsets)]);
                    if((bool)@$params['filter2']) unset($unsets[array_search($params['filter2'],$unsets)]);
                    if((bool)@$params['filter3']) unset($unsets[array_search($params['filter3'],$unsets)]);

                    break;

                case '2':
                    if((bool)@$params['filter']) unset($unsets[array_search($params['filter'],$unsets)]);
                    else unset($unsets[array_search('data',$unsets)]);

                    break;

                case '3':
                    if((bool)@$params['filter']) unset($unsets[array_search($params['filter'],$unsets)]);
                    else unset($unsets[array_search('data',$unsets)]);

                    break;

                case '5':
                    $unsets[] = 'alias';
                    $unsets[] = 'endereco';
                    $unsets[] = 'bairro';
                    $unsets[] = 'uf';
                    $unsets[] = 'cep';
                    $unsets[] = 'tel';
                    $unsets[] = 'email';
                    $unsets[] = 'site';
                    $unsets[] = 'body';
                    $unsets[] = 'body2';
                    $unsets[] = 'allow_files';
                    $unsets[] = 'allow_photos';
                    $unsets[] = 'foto_path';
                    $unsets[] = 'foto_path2';
                    $unsets[] = 'status_id';
                    $unsets[] = 'data';
                    $unsets[] = 'data_exp';
                    $unsets[] = 'data_cad';
                    $unsets[] = 'data_edit';
                    $unsets[] = 'user_cad';
                    $unsets[] = 'user_edit';

                    break;
            }
            if(count($unsets)) foreach($unsets as $u) if(isset($row[$u])) unset($row[$u]);

            // arrumando titulos
            $r = array();

            foreach($row as $k => $v){
                switch($k){
                    case 'data':
                    case 'data_edit':
                    case 'data_cad':
                        $r['Data'] = Is_Date::am2br($v);
                        break;
                    case 'count_records':
                        $r['Cadastros'] = $v;
                        break;
                    case 'count_access':
                    case 'counter':
                        $r['Acessos'] = $v;
                        break;
                    case 'atividade_profissional_id':
                        $r['Atividade Profissional'] = utf8_encode($this->atividade_profissional->getAtividade($v));
                        break;
                    case 'escolaridade_id':
                        $r['Escolaridade'] = utf8_encode($this->escolaridade->getEscolaridade($v));
                        break;
                    case 'filhos':
                        $r['Possui filhos'] = $v==1?'Sim':'Não';
                        break;
                    case 'sexo':
                        $r['Sexo'] = $v==1?'Masculino':'Feminino';
                        break;
                    case 'cidade':
                        // $cidade = $this->cidades->fetchRow('cod_cidades="'.$v.'"');
                        // $c = $cidade ? utf8_encode($cidade->nome) : '(Não especificado)';
                        // $r['Cidade'] = $c;
                        break;
                    case 'tema':
                        $r['Item'] = utf8_decode($v);
                        break;
                    case 'tipo':
                        ($this->relatorio_tipo==5) ?
                            $r['Area'] = $this->tipos_ads[$v]:
                            $r['Tipo'] = utf8_decode($v);
                        break;
                    case 'titulo':
                        ($this->relatorio_tipo==5) ?
                            $r['Anuncio'] = utf8_decode($v):
                            $r['Titulo'] = utf8_decode($v);
                        break;
                    default:
                        $k = ucwords(str_replace(array('_id','_','-'),' ',$k));
                        $v = utf8_decode($v);
                        $r[$k] = $v;
                }
            }

            $row = $r;
        }

        // _d($rows);
        return $rows;
    }

    public function getReportAction()
    {
        $params = $this->_request->getParams();

        try {
            return array(
                'msg' => 'Relatório carregado',
                'rows' => $this->relatorio->getReport($params['report'],$params)
            );
        }catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}
