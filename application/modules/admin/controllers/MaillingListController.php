<?php

class Admin_MaillingListController extends ZendPlugin_Controller_Ajax
{
    public function init()
    {
        Admin_Model_Login::checkAuth($this);
        
        $this->view->titulo = "MAILLING LIST";
        if($this->_hasParam('tipo')) $this->view->titulo.= ' - '.Application_Model_Db_Arquivos::tipo($this->_getParam('tipo'));
        $this->view->section = $this->section = "mailling-list";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
    }

    public function indexAction()
    {
        $table = new Application_Model_Db_Mailling();
        $tipo = ($this->_hasParam('tipo')) ? $this->_getParam('tipo') : 1;
        $arq = ($this->_hasParam('arquivo_id')) ? $this->_getParam('arquivo_id') : null;
        $order = null;
        $group = null;
        $where = null;
        $wheres = array();
        if($tipo) $wheres[] = 'tipo = "'.$tipo.'"';
        if($arq) $wheres[] = 'arquivo_id = "'.$arq.'"';
        
        if($this->_hasParam('search-by')){
            // $post = $this->_request->getPost();
            $post = $this->_request->getParams();
            $post['search-txt'] = Is_Date::br2am($post['search-txt']);

            $wheres[] = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $where = implode(' and ', $wheres);
            $order = $post['search-by'];
            
            // $lines = $table->fetchAll($where,$order,20,0);
        } else {
            $where = count($wheres) ? implode(' and ', $wheres) : null;
            // $lines = $table->fetchAll($where,array("data_cad desc"));
            $order = array("data_cad desc");
        }

        if($tipo==3){
            $group = 'm.email';
        }

        $lines = $table->q(
            'select m.*, '.
                'case area when 1 then "Newsletter" '.
                    'when 3 then "Publicação" '.
                    'else "Artigo" '.
                    'end as area, '.
                (($tipo==3) ?
                    'a.titulo arquivo ':
                    'a.descricao arquivo ').
            'from mailling m '.
            (($tipo==3) ?
                'left join publicacoes a on a.id = m.arquivo_id ' :
                'left join arquivos a on a.id = m.arquivo_id ').
            ($where ? 'where '.$where : '').' '.
            ($group ? 'group by '.((is_array($group))?implode(', ', $group):$group) : '').' '.
            ($order ? 'order by '.((is_array($order))?implode(', ', $order):$order) : '').' '.
            'limit 1000'
        );
        // $lines = Is_Array::utf8DbResult($lines);
        // _d($lines);
        
        $this->view->lines = $lines;
        $this->view->tipo = $tipo;

        $iframe = ($this->_hasParam('iframe')) ? $this->_getParam('iframe') : null;
        if($iframe) {
            $this->view->titulo = 'Mailling';
            // if($tipo==3) $this->view->titulo.= ' / Downloads';
        }
    }

    public function exportAction()
    {
        $this->indexAction();
        $rows = (array)$this->view->lines;
        $params = $this->_request->getParams();
        $this->filename = Is_Str::toUrl('Mailling List'.' - '.SITE_NAME);

        foreach($rows as &$row){
            // retirando params desnecessarios
            $row = (array)$row;

            $unsets = 'id,nascimento,sexo,data_cad,data_edit,promocao_id,vencedor,telefone,tipo,arquivo_id,tema_id';
            $unsets = explode(',',$unsets);

            // if((bool)@$params['filter']) unset($unsets[array_search($params['filter'],$unsets)]);
            // else unset($unsets[array_search('data',$unsets)]);
            // unset($unsets[array_search('nome',$unsets)]);

            if(count($unsets)) foreach($unsets as $u) if(isset($row[$u])) unset($row[$u]);

            // arrumando titulos
            $r = array();

            foreach($row as $k => $v){
                switch($k){
                    case 'data':
                        $r['Data'] = Is_Date::am2br($v);
                        break;
                    case 'count_records':
                        $r['Cadastros'] = $v;
                        break;
                    case 'count_access':
                        $r['Acessos'] = $v;
                        break;
                    case 'atividade_profissional_id':
                        $r['Atividade Profissional'] = utf8_encode($this->atividade_profissional->getAtividade($v));
                        break;
                    case 'escolaridade_id':
                        $r['Escolaridade'] = utf8_encode($this->escolaridade->getEscolaridade($v));
                        break;
                    case 'filhos':
                        $r['Possui filhos'] = $v==1?'Sim':'Não';
                        break;
                    case 'sexo':
                        $r['Sexo'] = $v==1?'Masculino':'Feminino';
                        break;
                    case 'cidade':
                        $cidade = $this->cidades->fetchRow('cod_cidades="'.$v.'"');
                        $c = $cidade ? utf8_encode($cidade->nome) : '(Não especificado)';
                        $r['Cidade'] = $c;
                        break;
                    case 'telefone':
                        $r['Telefone'] = Is_Format::tel($v);
                        break;
                    default:
                        $k = ucwords(str_replace(array('_id','_','-'),' ',$k));
                        $r[$k] = utf8_decode($v);
                }
            }

            $row = $r;
        }

        // _d($rows);
        return $rows;
    }
}

