<?php

class Admin_FornecedoresController extends ZendPlugin_Controller_Ajax
{
    public $tipo  = 'vp';
    public $tipos = array(
        'vp' => 'Veiculos pesados',
        'cr' => 'Carregadores',
        'ps' => 'Paineis solares',
        'pt' => 'Patrocinadores',
        'mt' => 'Montadoras',
    );

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        $tipo = ($this->_hasParam('tipo')) ? $this->_getParam('tipo') : null;
        if($tipo){
            $this->tipo = $tipo;
            $this->view->controller = 'fornecedores';
        }
        $this->view->tipo = $this->tipo;
        
        $this->view->titulo = (in_array($this->tipo, array('pt','mt'))) ? '' : "FORNECEDORES DE ";
        $this->view->titulo.= strtoupper($this->tipos[$this->tipo]);
        $this->view->section = $this->section = "fornecedores";
        $this->view->section2 = $this->section2 = 'fornecedores'.(($tipo) ? '-'.$tipo : '');
        // $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $url = $this->_request->getBaseUrl()."/admin/".$this->section.(($tipo) ? '-'.$tipo : '').'/';
        $this->view->url = $this->_url = $url;
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        // _d($this->img_path);

        // models
        $this->fornecedores = new Application_Model_Db_Fornecedores();
        $this->con = $this->fornecedores;
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));

        $this->view->MAX_FOTOS = 1;
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));

        Admin_Model_Login::setControllerPermissions($this,$this->section);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        $where = 'tipo = "'.$this->tipo.'" ';
        // $order = 'data_edit desc';
        $order = 'titulo';
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where.= 'and '.$post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->fornecedores->fetchAll($where,$order,$limit,$offset);
            
            $total = $this->view->total = $this->fornecedores->count($where);
        } else {
            $rows = $this->fornecedores->fetchAll($where,$order,$limit,$offset);
            $total = $this->view->total = $this->fornecedores->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Fornecedores();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->fornecedor_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = 1;//$data['allow_photos'];

            $carcon_list = array_map(function($ccl){
                return $ccl->produto_id;
            }, $this->con->s('fornecedores_produtos','*','fornecedor_id="'.$this->fornecedor_id.'"'));
            $this->view->carcon_list = $carcon_list;
            // _d($carcon_list);
            $car_list = $this->con->s('produtos','id,titulo','tipo=1','titulo');
            $this->view->car_list = $car_list;
            // _d($car_list);

            $this->view->row = (object)$data;
        } else {
            // $form->removeElement('body');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1','tipo'=>$this->tipo);
        }

        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->fornecedores->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            $data['body']  = isset($data['body']) ? cleanHtml($data['body']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            if(APPLICATION_ENV!='development1') $data = array_map('utf8_decode',$data);

            if((bool)trim($data['site'])) $data['site'] = check_url_http($data['site']);
            if((bool)trim($data['cep'])) $data['cep'] = Is_Cpf::clean($data['cep']);
            if((bool)trim($data['tel'])) $data['tel'] = Is_Cpf::clean($data['tel']);
            if((bool)trim($data['uf'])) $data['uf'] = strtoupper($data['uf']);
            // _d($data);
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->fornecedores->update($data,'id='.$id) : $id = $this->fornecedores->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section2.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            // $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
            $this->_redirect(URL.'/admin/'.$this->section2.'/'.($row ? 'edit/'.$row->id : 'new' ));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->fornecedores->fetchRow('id='.$id);
        // $row   = $this->fornecedores->fetchRow('id="'.$id.'" and tipo = "'.$this->tipo.'"');

        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        if($row->tipo!=$this->tipo){ return $this->_redirect(URL.'/admin/'.$this->section.'-'.$row->tipo.'/edit/'.$row->id.'/');return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->fornecedores->delete("id=".(int)$id);
            return array('msg'=>'Registro excluido.');
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('fornecedores_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->fornecedor_id)){
            $select->where('f2.fornecedor_id = ?',$this->fornecedor_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/fornecedores/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $fornecedores_fotos = new Application_Model_Db_FornecedoresFotos();
            $fornecedor_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $fornecedores_fotos->insert(array("foto_id"=>$foto_id,"fornecedor_id"=>$fornecedor_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();
        
        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = new Application_Model_Db_Fotos();
        
        // $check_uploads = array();
        // for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i;
        // $check_uploads = implode(',',$check_uploads);
        $check_uploads = 'foto_id,foto_id2';

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
                $key = str_replace('foto_id', 'foto_path', $cu);
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);

                    // $fotos[$cu] = $table->insert(array('path'=>$rename));
                    $this->fornecedores->update(array($key=>$rename),'id = '.$data['fornecedor_id']);
                } else {
                    $err = $error;
                    if(!ENV_PRO) $err.= "<br/>\n".var_dump($upload->getErrors());
                    if(ENV_DEV) _d($err);
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/fornecedores-'.$data['fornecedor_tipo'].'/edit/'.$data['fornecedor_id']);
                }
            }
        }
        
        // if(count($fotos)) {
        //     $this->fornecedores->update($fotos,'id = '.$data['produto_id']);
        // }

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/fornecedores-'.$data['fornecedor_tipo'].'/edit/'.$data['fornecedor_id']);
    }

    public function carconAddAction()
    {
        $this->carcon = new Application_Model_Db_FornecedoresProdutos();
        $car = ($this->_hasParam('car')) ? $this->_getParam('car') : null;
        $con = ($this->_hasParam('con')) ? $this->_getParam('con') : null;
        if(!$car||!$con) return array('error'=>'Acesso negado');
        try {
            $this->carcon->insert(array(
                'fornecedor_id' => $con,
                'produto_id' => $car,
            ));
        } catch (Exception $e) {
            $err = 'Erro ao salvar';
            if(ENV_DEV) $err.= "\n".$e->getMessage();
            return array('error'=>$err);
        }
        return array('msg'=>'Adicionado');
    }

    public function carconDelAction()
    {
        $this->carcon = new Application_Model_Db_FornecedoresProdutos();
        $car = ($this->_hasParam('car')) ? $this->_getParam('car') : null;
        $con = ($this->_hasParam('con')) ? $this->_getParam('con') : null;
        if(!$car||!$con) return array('error'=>'Acesso negado');
        try {
            $this->carcon->delete(
                'fornecedor_id = "'.$con.'" '.
                'and produto_id = "'.$car.'"'
            );
        } catch (Exception $e) {
            $err = 'Erro ao salvar';
            if(ENV_DEV) $err.= "\n".$e->getMessage();
            return array('error'=>$err);
        }
        return array('msg'=>'Removido');
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}