<?php

class Admin_ConcessionariasController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "CONCESSIONÁRIAS";
        $this->view->section = $this->section = "concessionarias";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;
        // _d($this->img_path);

        // models
        $this->concessionarias = new Application_Model_Db_Concessionarias();
        $this->con = $this->concessionarias;
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));

        $this->view->MAX_FOTOS = 1;
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));

        Admin_Model_Login::setControllerPermissions($this,$this->section);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        // $order = 'data_edit desc';
        $order = 'titulo';
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->concessionarias->fetchAll($where,$order,$limit,$offset);
            
            $total = $this->view->total = $this->concessionarias->count($where);
        } else {
            $rows = $this->concessionarias->fetchAll(null,$order,$limit,$offset);
            $total = $this->view->total = $this->concessionarias->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Concessionarias();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->concessionaria_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = 1;//$data['allow_photos'];

            $carcon_list = array_map(function($ccl){
                return $ccl->produto_id;
            }, $this->con->s('concessionarias_produtos','*','concessionaria_id="'.$this->concessionaria_id.'"'));
            $this->view->carcon_list = $carcon_list;
            // _d($carcon_list);
            $car_list = $this->con->s('produtos','id,titulo','tipo=1','titulo');
            $this->view->car_list = $car_list;
            // _d($car_list);

            $marcon_list = array_map(function($mcl){
                return $mcl->marca_id;
            }, $this->con->s('concessionarias_marcas','*','concessionaria_id="'.$this->concessionaria_id.'"'));
            $this->view->marcon_list = $marcon_list;
            // _d($marcon_list);
            $mar_list = $this->con->s('marcas','id,descricao',null,'descricao');
            $this->view->mar_list = $mar_list;
            // _d($mar_list);
        } else {
            // $form->removeElement('body');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->concessionarias->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            $data['body']  = isset($data['body']) ? cleanHtml($data['body']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            if(APPLICATION_ENV!='development1') $data = array_map('utf8_decode',$data);

            if((bool)trim($data['site'])) $data['site'] = check_url_http($data['site']);
            if((bool)trim($data['cep'])) $data['cep'] = Is_Cpf::clean($data['cep']);
            if((bool)trim($data['tel'])) $data['tel'] = Is_Cpf::clean($data['tel']);
            if((bool)trim($data['uf'])) $data['uf'] = strtoupper($data['uf']);
            // _d($data);

            // pega dados de latitude + longitude
            $updateLatLng = !$row;
            if($row) if(!$row->lat || !$row->lng) $updateLatLng = true;
            if($row) if((bool)trim($data['cep']) && $row->cep != $data['cep']) $updateLatLng = true;
            // _d($updateLatLng);
            if($updateLatLng) if($geo = gmapsLatLng($data['cep'])){
                $data['lat'] = $geo['lat'];
                $data['lng'] = $geo['lng'];
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->concessionarias->update($data,'id='.$id) : $id = $this->concessionarias->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            // $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
            $this->_redirect(URL.'/admin/'.$this->section.'/'.($row ? 'edit/'.$row->id : 'new' ));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->concessionarias->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->concessionarias->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('concessionarias_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->concessionaria_id)){
            $select->where('f2.concessionaria_id = ?',$this->concessionaria_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        // $max_size = '5120'; // '2048'
        $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/concessionarias/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $concessionarias_fotos = new Application_Model_Db_ConcessionariasFotos();
            $concessionaria_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $concessionarias_fotos->insert(array("foto_id"=>$foto_id,"concessionaria_id"=>$concessionaria_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function marconAddAction()
    {
        $this->marcon = new Application_Model_Db_ConcessionariasMarcas();
        $mar = ($this->_hasParam('mar')) ? $this->_getParam('mar') : null;
        $con = ($this->_hasParam('con')) ? $this->_getParam('con') : null;
        if(!$mar||!$con) return array('error'=>'Acesso negado');
        try {
            $this->marcon->insert(array(
                'concessionaria_id' => $con,
                'marca_id' => $mar,
            ));
        } catch (Exception $e) {
            $err = 'Erro ao salvar';
            if(ENV_DEV) $err.= "\n".$e->getMessage();
            return array('error'=>$err);
        }
        return array('msg'=>'Adicionado');
    }

    public function marconDelAction()
    {
        $this->marcon = new Application_Model_Db_ConcessionariasMarcas();
        $mar = ($this->_hasParam('mar')) ? $this->_getParam('mar') : null;
        $con = ($this->_hasParam('con')) ? $this->_getParam('con') : null;
        if(!$mar||!$con) return array('error'=>'Acesso negado');
        try {
            $this->marcon->delete(
                'concessionaria_id = "'.$con.'" '.
                'and marca_id = "'.$mar.'"'
            );
        } catch (Exception $e) {
            $err = 'Erro ao salvar';
            if(ENV_DEV) $err.= "\n".$e->getMessage();
            return array('error'=>$err);
        }
        return array('msg'=>'Removido');
    }

    public function carconAddAction()
    {
        $this->carcon = new Application_Model_Db_ConcessionariasProdutos();
        $car = ($this->_hasParam('car')) ? $this->_getParam('car') : null;
        $con = ($this->_hasParam('con')) ? $this->_getParam('con') : null;
        if(!$car||!$con) return array('error'=>'Acesso negado');
        try {
            $this->carcon->insert(array(
                'concessionaria_id' => $con,
                'produto_id' => $car,
            ));
        } catch (Exception $e) {
            $err = 'Erro ao salvar';
            if(ENV_DEV) $err.= "\n".$e->getMessage();
            return array('error'=>$err);
        }
        return array('msg'=>'Adicionado');
    }

    public function carconDelAction()
    {
        $this->carcon = new Application_Model_Db_ConcessionariasProdutos();
        $car = ($this->_hasParam('car')) ? $this->_getParam('car') : null;
        $con = ($this->_hasParam('con')) ? $this->_getParam('con') : null;
        if(!$car||!$con) return array('error'=>'Acesso negado');
        try {
            $this->carcon->delete(
                'concessionaria_id = "'.$con.'" '.
                'and produto_id = "'.$car.'"'
            );
        } catch (Exception $e) {
            $err = 'Erro ao salvar';
            if(ENV_DEV) $err.= "\n".$e->getMessage();
            return array('error'=>$err);
        }
        return array('msg'=>'Removido');
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}