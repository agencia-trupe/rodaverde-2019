<?php

class Application_Model_Db_Categorias extends ZendPlugin_Db_Table
{
    protected $_name = "categorias";
    protected $default_order = 'ordem'; // ordem padrão para ordenação dos registros
    // protected $_foto_join_table = 'categorias_fotos'; // tabela de relação para registros de fotos
    // protected $_foto_join_table_field = 'categoria_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    
    public static $roles = array(
        '0' => 'Todos',
        '1' => 'Usuários',
        '2' => 'Clientes'
    );

    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Produtos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'categoria_id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($text='descricao',$combo=false,$order=NULL,$where=NULL)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll(null,'ordem');
        
        foreach($rows as $row){
            $values[$row->id] = utf8_encode($row->$text);
        }
        
        return $values;
    }
    
    /**
     * Retorna categoria com seus produtos e imagens com base no alias
     * @alias pode ser string (alias) ou numérico (id)
     *
     * @param string|int $alias   - alias ou id da categoria
     * @param string     $where_c - where categoria
     * @param string     $where_p - where produtos
     * @param array      $order   - order produtos
     * @param int        $limit   - limite do select - default null
     * @param int        $offset  - offset do select - default null
     *
     * @return object|bool - objeto contendo a categoria com produtos
     *                       false se não for encontrado
     */
    public function getWithProdutos($alias,$where_c=null,$where_p=null,$order='p.id desc',$limit=null,$offset=null)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        $where_categoria = $column.'="'.$alias.'"';
        if((bool)$where_c) $where_categoria.= ' and ('.$where_c.')';

        if(!$categoria = $this->fetchRow($where_categoria)) return false;

        $_produtos = new Application_Model_Db_Produtos();
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos_categorias as pc',array('produto_id','categoria_id'))
               ->joinLeft('produtos as p','p.id = pc.produto_id',array('*'))
               ->where('pc.categoria_id = '.$categoria->id);

        if((bool)$where_p) $select->where($where_p);
        if((bool)$order)   $select->order($order);
        if((bool)$limit)   $select->limit($limit,$offset);

        $produtos = $select->query()->fetchAll();
        $produtos = array_map('Is_Array::utf8All',$produtos);
        $produtos = array_map('Is_Array::toObject',$produtos);
        $produtos = $_produtos->checkDesconto($produtos);
        $produtos = $_produtos->getFotos($produtos,true);

        $object = Is_Array::utf8DbRow($categoria);
        $object->produtos = $produtos;

        // pega fotos da categoria
        if($categoria->foto_id || $categoria->foto2_id) {
            $f = ($categoria->foto_id) ?
                $this->s1('fotos','path,titulo','id="'.$categoria->foto_id.'"') :
                null;
            $f2 = ($categoria->foto2_id) ?
                $this->s1('fotos','path,titulo','id="'.$categoria->foto2_id.'"') :
                null;

            if($f) {
                $alias = '';
                if((bool)trim($f->titulo)) $alias = Is_Str::toUrl($f->titulo).'-';
                else if($categoria->alias) $alias = $categoria->alias.'-';
                $f->path = $alias.$f->path;
            }
            if($f2) {
                $alias = '';
                if((bool)trim($f2->titulo)) $alias = Is_Str::toUrl($f2->titulo).'-';
                else if($categoria->alias) $alias = $categoria->alias.'-';
                $f2->path = $alias.$f2->path;
            }

            $object->foto_path = $f->path;
            $object->foto2_path = $f2->path;
        }
        
        $object->count = $limit ? $this->count_getWithProdutos($object->id,$where_p) : count($object->produtos);
        // _d($object);
        return $object;
    }
    
    /**
     * Retorna total de produtos na categoria 
     */
    public function count_getWithProdutos($categoria_id,$where_p=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos as p',array(
                new Zend_Db_Expr('COUNT(*) as count')
            ))
            ->joinRight('produtos_categorias as pc','pc.produto_id = p.id',array())
            ->where('pc.categoria_id = ?',$categoria_id);
        
        if((bool)$where_p){ $select->where($where_p); }
        // _d($select->query()->__toString());
        $count = $select->query()->fetchAll();
        return $count[0]['count'];
    }
    
    /**
     * Retorna as fotos da categoira
     *
     * @param int $id - id da categoria
     *
     * @return array - rowset com fotos da categoria
     */
    public function getFotos($id)
    {
        if(!$promocao = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($promocao_fotos = $promocao->findDependentRowset('Application_Model_Db_Fotos')){
            foreach($promocao_fotos as $promocao_foto){
                $fotos[] = Is_Array::utf8DbRow($promocao_foto->current());
            }
        }
        
        return $fotos;
    }
    
    /**
     * Retorna categorias pai
     *
     * @param mixed $categ - array  : lista de resultados para filtrar e retornar somente pais
     *                       object : objeto resultado para filtrar e retornar sua categoria pai
     *                       int    : id da subcategoria p/ retornar sua categoria pai
     *                       null   : retorna todas as categorias pai do db
     *                       
     * @param bool $parent_null - opcional - usar somente se for passado um rowset(array) como 1º argumento
     *                            false : retornará os pais dos registros com parent_id (padrão)
     *                            true  : retornará todos os pais do rowset
     * 
     * @return array - rowset com categorias pai
     */
    public function getParents($categ=null,$parent_null=false)
    {
        if(is_array($categ)){ // filtra resultSet
            $cats = array();
            
            foreach($categ as $cat){
                if($parent_null){
                    if(!(bool)$cat->categoria_id)
                        $cats[] = $cat;
                } else {
                    if((bool)$cat->categoria_id)
                        $cats[] = $this->checkParentFromRowset($cat->categoria_id,$categ);
                }
            }
        } else if(is_object($categ)){ // busca pai de row
            return (bool)$categ->categoria_id ? $this->fetchRow('id = '.$categ->categoria_id) : null;
        } else if(is_int($categ)){ // busca por id
            $row = $this->fetchRow('id = '.$categ);
            return (bool)$row->categoria_id ? $this->fetchRow('id = '.$row->categoria_id) : null;
        } else { // busca todos os pais
            $cats = $this->fetchAll('categoria_id is null');
        }
        
        return $cats;
    }
    
    /**
     * Checa qual item do resultset é pai
     *
     * @param int   $cat  - categoria_id (id do pai a procurar)
     * @param array $cats - resultset p/ filtrar pai
     */
    public function checkParentFromRowset($cat,$cats)
    {
        foreach($cats as $c){
            if($c->id == $cat){
                return $c;
            }
        }
    }
    
    /**
     * Retorna key/values dos pais
     *
     * @param array $cats - resultset de categorias_pai
     * @param array $first - primeiro(s) elementos do array retornado - Ex.: 0 => 'Selecione...'
     */
    public function getParentsKV($cats,$first=null,$optGroup=false)
    {
        $keyvalues = is_array($first) ? $first : array();
        $last = null;

        if($optGroup){
            foreach($cats as $cat){
                if(!strstr($cat->descricao,'- ')){
                    if(!isset($keyvalues[$cat->descricao])) $keyvalues[$cat->descricao] = array();
                    $last = $cat->descricao;
                } else {
                    if($last) {
                        if(isset($keyvalues[$last])) {
                            $keyvalues[$last][$cat->id] = $cat->descricao;
                        } else {
                            $keyvalues[$cat->id] = $cat->descricao;
                        }
                    } else {
                        $keyvalues[$cat->id] = $cat->descricao;
                    }
                }
            }
        } else {
            foreach($cats as $cat) $keyvalues[$cat->id] = $cat->descricao;
        }
        return $keyvalues;
    }
    
    /**
     * Retorna categorias filhas
     *
     * @param int $parent_id - id ou alias da categoria pai
     */
    public function getChildren($parent_id,$order='id')
    {
        if(!is_numeric($parent_id)){
            $parent = $this->fetchRow('alias = "'.$parent_id.'"');
            $parent_id = (bool)$parent ? $parent->id : 0;
        }

        return $this->fetchAll('categoria_id = "'.$parent_id.'"',$order);
    }
    
    /**
     * Retorna categorias com filhos
     * TODO: otimizar consulta
     *
     * @param string $where - where do select (padrão: categoria_id is null)
     * @param string $order - order do select (padrão: ordem)
     * @param string $ident - identação das subcategorias (padrão; - )
     */
    public function getWithChildren($where=null,$order=null,$ident=null)
    {
        if($where===null) $where = 'categoria_id is null';
        if($order===null) $order = 'ordem';
        if($ident===null) $ident = '- ';
        
		$cats = array();
        $_cats = $this->fetchAll($where,$order);
        $_cats = count($_cats) ? Is_Array::utf8DbResult($_cats) : $cats;
		
        if($_cats) foreach($_cats as $_cat){
            $cats[] = $_cat;
			$children = $this->getChildren($_cat->id);
			if(count($children)){
				$children = Is_Array::utf8DbResult($children);
				foreach($children as &$child){
					$child->descricao = $ident.$child->descricao;
					$cats[] = $child;
				}
            }
		}
        
        return $cats;
    }
    
    /**
     * Retorna produtos de categorias em promoção
     */
    public function getProductsInPromo($limit=null)
    {
        $this->produtos = new Application_Model_Db_Produtos();
        $promos = $this->fetchAll('desconto is not null and desconto <> "0" and id <> '.VALEPRESENTEID,null,$limit);
        $produtos = array();
        
        if(count($promos)){
            $promos = Is_Array::utf8DbResult($promos);
            
            foreach($promos as &$promo){
                $prods = $this->getWithProdutos($promo->alias,$limit,null,true);
                if(count($prods->produtos)){
                    foreach($prods->produtos as $p) array_push($produtos,$p);
                }
            }
        }
        
        return $produtos;
    }

    public function getRoles($id=null)
    {
        return $id ? self::$roles[$id] : self::$roles;
    }

    // replacement em método padrão para pegar fotos com alias
    public function fetchAllWithPhoto($where=null,$order=null,$limit=null,$offset=null,$params=array())
    {
        $rows = parent::fetchAllWithPhoto($where,$order,$limit,$offset,$params);
        
        // assimilando alias ao foto->path
        foreach($rows as $row) if($row->foto_id) {
            if(!$f = $this->s1('fotos','path,titulo','id='.$row->foto_id)) continue;
            $alias = '';
            if((bool)trim($f->titulo)) $alias = Is_Str::toUrl($f->titulo).'-';
            else if($row->alias) $alias = $row->alias.'-';
            $f->path = $alias.$f->path;
            $row->foto_path = $f->path;
        }

        return $rows;
    }

}