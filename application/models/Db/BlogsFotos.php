<?php

class Application_Model_Db_BlogsFotos extends Zend_Db_Table
{
    protected $_name = "blogs_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Blogs','Application_Model_Db_BlogsPosts');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Blogs' => array(
            'columns' => 'blog_post_id',
            'refTableClass' => 'Application_Model_Db_Blogs',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_BlogsPosts' => array(
            'columns' => 'blog_post_id',
            'refTableClass' => 'Application_Model_Db_BlogsPosts',
            'refColumns'    => 'id'
        )
    );
}
