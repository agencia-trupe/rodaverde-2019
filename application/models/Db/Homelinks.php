<?php

class Application_Model_Db_Homelinks extends ZendPlugin_Db_Table 
{
    protected $_name = "homelinks";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos do homelink
     *
     * @param int $id - id do homelink
     *
     * @return array - rowset com fotos do homelink
     */
    public function getFotos($id)
    {
        if(!$homelink = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($homelink_fotos = $homelink->findDependentRowset('Application_Model_Db_HomelinksFotos')){
            foreach($homelink_fotos as $homelink_foto){
                $fotos[] = Is_Array::utf8DbRow($homelink_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    public function getLast()
    {
        $homelink = $this->getLasts(1);
        return $homelink[0];
    }

    public function getLasts($limit=3,$order='data_edit desc')
    {
        $_homelinks = $this->fetchAll('status_id=1',$order,$limit);
        
        if(count($_homelinks)){
            $homelinks = Is_Array::utf8DbResult($_homelinks);
            
            foreach($homelinks as &$homelink){
                $homelink->fotos = $this->getFotos($homelink->id);
                $homelink->foto = @$homelink->fotos[0]->path;
            }
            
            return $homelinks;
        }

        return null;
    }
    
}