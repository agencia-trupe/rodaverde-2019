<?php

class Application_Model_Db_Clicks extends ZendPlugin_Db_Table
{
    protected $_name = "clicks";
    
    /**
     * Referências
     */
    // protected $_dependentTables = array('Application_Model_Db_Produtos');
    
    // protected $_referenceMap = array(
    //     'Application_Model_Db_Produtos' => array(
    //         'columns' => 'produto_id',
    //         'refTableClass' => 'Application_Model_Db_Produtos',
    //         'refColumns'    => 'id'
    //     )
    // );

    public function add($area,$id,$tema=null,$params=array()){
        $url = FULL_URL;
        if((bool)@$_SERVER['HTTP_REFERER']) $url = $_SERVER['HTTP_REFERER'];
        if(array_key_exists('url', $params)) $url = $params['url'];

        $data = array(
            'area_id' => $area,
            'tema_id' => $id,
            'tema'    => utf8_decode($tema),
            'data'    => date('Y-m-d H:i:s'),
            'url'     => $url,
            'ip'      => ip2long(Is_Server::getIp()),
        );
        // _d($data);
        $this->insert($data);

        // atualiza table com clicks para o tema
        switch ($area) {
            case 3: // fornecedores - veículos pesados
            case 4: // fornecedores - carregadores
            case 5: // fornecedores - painéis solares
                $this->q('update fornecedores '.
                    'set clicks = clicks + 1 '.
                    'where id = "'.$id.'"');
                break;
            case 2: // concessionarias
                $this->q('update concessionarias '.
                    'set clicks = clicks + 1 '.
                    'where id = "'.$id.'"');
                break;
            case 0: // ads
                $this->q('update ads '.
                    'set clicks = clicks + 1 '.
                    'where id = "'.$id.'"');
                break;
        }
        
        return $this;
    }
}
