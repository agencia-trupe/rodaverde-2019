<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        $this->router = new Zend_Controller_Router_Rewrite();
		$this->request =  new Zend_Controller_Request_Http();
		$this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');
        
        // if($this->request->getModuleName()=="default"){
        if($this->request->getControllerName()!="admin"){
            // dados da empresa
            $dados_empresa = db_table('dados_empresa');
            $this->view->dados_empresa = Is_Array::utf8DbRow(
                $dados_empresa->fetchRow('id = 1')
            );
            $this->view->dados = $this->view->dados_empresa;

            // anuncios
            $ads = db_table('ads');
            $where_ads = 'status_id = 1 ';
            $where_ads.= 'and data_exp >= "'.date('Y-m-d H:i:s').'" ' ;
            $where_ads.= 'and tipo = "'.($this->request->getControllerName()=='index'?0:1).'" ' ;
            $limit_ads = ($this->request->getControllerName()=='index'?2:4);
            $this->view->ads = Is_Array::utf8DbRows(
                $ads->fetchAll($where_ads,null,$limit_ads)
            );
            // _d($this->view->ads);
        }
        
        if($this->request->getControllerName()=="admin"){
            $this->layout->setLayout("admin");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        }
		
        if(array_key_exists('busca',$this->request->getParams())){
            $this->view->busca = $this->request->getParam('busca');
        }

        // configurações
        $this->site_config = new Zend_Session_Namespace(SITE_NAME.'_site_config');
        if(!isset($this->site_config->lang))   $this->site_config->lang   = DEFAULT_LANGUAGE;

        // translate
        if(isset($_GET['lang'])){
            $lang = trim(addslashes($_GET['lang']));

            if((bool)$lang && in_array($lang,explode(',',SITE_LANGUAGES))){
                $this->site_config->lang = $lang;
            } else {
                $this->site_config->lang = DEFAULT_LANGUAGE;
            }
        }

        $locale = new Zend_Locale($this->site_config->lang);
        Zend_Registry::set('Zend_Locale', $locale);
        
        $translate = new Zend_Translate(
            array(
                'adapter' => 'csv',
                'content' => APPLICATION_PATH.'/languages',
                // 'locale'  => 'auto',
                'scan' => Zend_Translate::LOCALE_FILENAME
            )
        );
        $translate->setLocale($locale);
        if (!$translate->isAvailable($locale->getLanguage())) {
            $translate->setLocale(DEFAULT_LANGUAGE);
        }
        // _d($locale->toString().' - '.$locale->getLanguage());
        
        Zend_Registry::set('Zend_Translate', $translate);
        Zend_Form::setDefaultTranslator($translate);

        $this->view->translate_object = $translate;
        $this->view->site_config = $this->site_config;
        $this->view->lang = $this->site_config->lang;
        $this->lang = $this->site_config->lang;
        // _d(array('t'=>$translate->getLocale(),'c'=>$this->site_config));
        // _d(array($this->site_config));
    }
    
    protected function _initLayoutConfigs(){
        $this->view->doctype('XHTML1_STRICT');
		
		switch($this->request->getControllerName()){
			case 'admin':
				$actionName = $this->request->getActionName();
				$actions = explode('/',$this->request->getRequestUri());
				$curAction = end($actions);
				$s = array_search($actionName,$actions);
				$action = $curAction!=$actions[$s]?$curAction:'index';
				//_d($action);
				$this->view->action = $action;
				$this->view->controller = $actionName;
				$this->view->module = $this->request->getControllerName();
				break;
			default:
				$this->view->action = $this->request->getActionName();
				$this->view->controller = $this->request->getControllerName();
				$this->view->module = $this->request->getModuleName();
		}
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        if($this->request->getControllerName()=="admin"){   
            $login = new Zend_Session_Namespace(SITE_NAME.'_login');         
            $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            $config = $config->toArray();
            // _d(new Zend_Navigation($config['admin-top']));
            $this->view->menu = new stdClass();

            // lista paginas
            $pag = new Application_Model_Db_Paginas();
            $rows_pags = array();//Is_Array::utf8DbResult($pag->fetchAll('status_id = 1',array('id')));
            
            if(count($rows_pags)){
                $config['admin-top']['paginas']['pages'] = array();

                foreach($rows_pags as $row_pag){
                    $page = array(
                        'label' => $row_pag->titulo,
                        'uri'   => URL.'/admin/paginas/edit/'.$row_pag->id
                    );
                    
                    $config['admin-top']['paginas']['pages']['pagina-'.$row_pag->alias] = $page;
                }
            }

            // lista parceiros
            $parc_pages = array(
                'parceirotipos' => array(
                    'label' => 'Gerenciar categorias',
                    'uri'   => URL.'/admin/parceirotipos',
                    'class' => 'bd-bot',
                ),
            );
            
            $tipos = array_map('utf8_decode', $pag->kva('parceirotipos','descricao',0,'ordem','alias not in ("vp","cr","ps")'));
            // _d($tipos);
            foreach($tipos as $tpK => $tpV) $parc_pages['parceiro-'.$tpK] = array(
                'label' => $tpV,
                'uri'   => URL.'/admin/parceiros?tipo='.$tpK,
            );
            
            $parc_pages['paineis-solares'] = array(
                'label' => 'Painéis Solares',
                'uri'   => URL.'/admin/fornecedores-ps',
                'class' => 'bd-top',
            );
            $parc_pages['carregadores'] = array(
                'label' => 'Carregadores',
                'uri'   => URL.'/admin/fornecedores-cr',
            );
            $parc_pages['concessionarias'] = array(
                'label' => 'Concessionárias',
                'uri'   => URL.'/admin/concessionarias',
            );
            $config['admin-top']['parceiros']['pages'] = $parc_pages;
            // _d($parc_pages);

            // lista mensagens contato
            $msg = new Application_Model_Db_Mensagens();
            $msgs = $msg->fetchAll('status_id=1','id');

            if ($msgs) {
                $menu_msgs = array();
                $menu_msgs['mensagens-todas'] = array(
                    'label' => 'Todas',
                    'uri'   => URL.'/admin/mensagens-contato',
                );

                foreach ($msgs as $m) {
                    $menu_msgs['mensagens-'.$m->id] = array(
                        'label' => $m->titulo,
                        'uri'   => URL.'/admin/mensagens-contato/?search-by=mensagem_id&search-txt='.$m->id,
                        // 'uri'   => URL.'/admin/mensagens-contato/index/search-by/mensagem_id/search-txt/'.$m->id,
                    );
                }

                // _d($menu_msgs);
                $config['admin-top']['mensagens']['pages']['recebidas']['class'] = 'has-sub-sub';
                $config['admin-top']['mensagens']['pages']['recebidas']['pages'] = $menu_msgs;
            }

            $menu_admin_top = $config['admin-top'];
            self::configUrlPrefix($menu_admin_top);

            $this->view->menu->admin_top  = new Zend_Navigation($menu_admin_top);
        } else {
            $table = new Application_Model_Db_BlogsPosts();

            // $this->view->blog_last_posts = $table->getLastPosts(3);

            if(($this->request->getControllerName()=="blog") ||
               ($this->request->getControllerName()=="tendencias-mobilidade") ||
               ($this->request->getControllerName()=="blog-noticias")) {
                $config_categ = array();
                $blog_id = $this->request->getControllerName()=='blog-noticias' ? 2 : 1;
                $blog_alias = $this->request->getControllerName();
                $categs_rows = $table->q(
                    'select c.*, count(p.id) as post_count from categorias_blog c '.
                    'left join blogs_posts p on p.categoria_id = c.id '.
                    'where c.status_id = 1 and c.blog_id = '.$blog_id.' '.
                    'group by c.id '.
                    'order by c.ordem '
                );
                // $categs_rows = $categs->fetchAll('status_id = 1','ordem');
                $this->view->categs_rows = $categs_rows;
                $i = 0;
                
                foreach($categs_rows as $categ){
                    $config_categ[$categ->alias] = array(
                        'label' => ($categ->{'descricao_'.$this->lang}),
                        'title' => ($categ->{'descricao_'.$this->lang}),
                        //'uri'   => (APPLICATION_ENV == 'development'?'/'.SITE_NAME.'/':'/').'categoria/'.$categ->alias.'/'
                        'id'    => 'categoria-'.$categ->id,
                        'uri'   => URL.'/'.$blog_alias.'/categoria/'.$categ->alias,
                        'alias' => $categ->alias,
                        'count' => $categ->post_count,
                        'pages' => array(
                            array(
                                'label' => ($categ->{'descricao_'.$this->lang}),
                                'uri'   => URL.'/blog/'.$blog_alias.'/'.$categ->alias,
                                'alias' => $categ->alias,
                                'count' => $categ->post_count,
                                'class' => 'submenu-title'
                            )
                        )
                    );
                }

                self::configUrlPrefix($config_categ);
                $config_categ_foot = $config_categ;
                foreach($config_categ_foot as &$categ){
                    unset($categ['pages']);
                }
                // _d($config_categ);

                $this->view->menu_blog = new stdClass();
                // $this->view->menu_blog->categorias_top = new Zend_Navigation(array_reverse($config_categ));
                $this->view->menu_blog->categorias  = new Zend_Navigation($config_categ);
                // $this->view->menu_blog->categorias_footer  = new Zend_Navigation($config_categ_foot);

                // $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
                // $menu_nav = $_menu_nav->toArray();
                // $this->view->menu->footer         = new Zend_Navigation($menu_nav['footer']);
            }
            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();
            $menu_nav_top = $menu_nav['top'];

            // prefixos
            self::configUrlPrefix($menu_nav_top);
            self::configUrlPrefix($menu_nav['footer']);
			
            $this->view->menu = new stdClass();
            $this->view->menu->top        = new Zend_Navigation($menu_nav_top);
            $this->view->menu->footer    = new Zend_Navigation($menu_nav['footer']);
			
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass($activeNav->getClass()." active");	
                }
            }
        }
	}

    function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' & !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recursão à função
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}